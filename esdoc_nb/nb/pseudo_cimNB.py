#!/usr/bin/env python
# Simple user notebook for collecting newCIM metadata
# BNL September 2014
#
import esdoc_nb.mp.core.lib.definitions as definitions
import esdoc_nb.mp.core.lib.factory as factory
import esdoc_nb.mp.core.lib.uml as uml

import pygtk

pygtk.require('2.0')
import gtk
import pseudo_view as vw
import pseudo_nbwidgets as nbw
import pseudo_tools as pt
import pseudo_csv as pcsv
import pseudo_help as ph
import pseudo_registry as pr
import pseudo_spreadsheet as pss

from collections import OrderedDict

from pseudo_storage import cimStorage

import os, copy

last_used_file = '.cimNB.lastUsed'

class indexedNB(gtk.Notebook):
    
    """ Provides a customised indexed by name notebook container """
    
    def __init__(self, *args, **kw):
        super(gtk.Notebook, self).__init__(*args, **kw)
        # local attributes follow
        self.widgets = {}
        self.decorate()

    def decorate(self):
        """ cimNB default decoration """
        self.set_border_width(5)
        self.set_tab_pos(gtk.POS_TOP)
        self.show_tabs = True
        self.show_border = True
        
    def add_page(self, content, key, title):
        """ Append some content, provide appropriate title, and index
        according to key """
        if key in self.widgets:
            raise ValueError('Page %s already exists' % key)
        else:
            self.widgets[key] = content
            self.append_page(content, gtk.Label(title))
            
    def replace_page(self, content, key, title):
        """ Replace the page with given key with the new content and title"""
        p = self.remove_by_key(key)
        self.widgets[key] = content
        self.insert_page(content, gtk.Label(title), p)
        self.set_current_page(p)
        if self.get_current_page() != p:
            raise KeyError('Attempt to set page to %s failed' % p)
        
    def remove_by_key(self, key):
        """ Remove page by key, and return old page number"""
        widget_to_go = self.widgets[key]
        p=self.page_num(widget_to_go)
        self.remove_page(p)
        del self.widgets[key]
        return p
        
    def show(self):
        super(gtk.Notebook, self).show()

class collectionVBox(gtk.VBox):
    """ Provides a collection NB navigator for a specific sub-collection of documents
    """
    def __init__(self, changed_navigator_callback, view='designing'):
        """
        Instantiates a vbox to choose and display a sub-collection navigator
        :param changed_navigator_callback to expose changes upstream
        :param view: default sub collection view
        :return: None
        The calling routine should pack the cim navigator on the end of this box
        using the set_navigator method, then call show.
        """

        super(collectionVBox, self).__init__()

        self.subcollections = OrderedDict([
            ('designing',OrderedDict([
                ('Project', 'project'),
                ('NumericalExperiment', 'experiment'),
                ('NumericalRequirement', 'requirement'),
                ('Party', 'party'),
                ])),
            ('planning', OrderedDict([
                ('SimulationPlan', 'planned simulation'),
                ('Party', 'party'),
                ])),
            ('documenting runs', OrderedDict([
                ('Ensemble','ensemble'),
                ('Conformance','conformance'),
                ('Simulation','simulation'),
                ('Model', 'model'),
                ('Party','party'),
                ])),
            ('documenting models', OrderedDict([
                ('Model', 'model'),
                ('ScientificDomain','scientific domain'),
                ('Party', 'party'),
            ])),
            ('other', OrderedDict([
                ('Machine','machine'),
                ('Performance','performance'),
                ('Dataset','data set'),
                ('Party','party'),
                ('ExternalDocument', 'documents'),
                ])),
        ])
        assert view in self.subcollections

        self.spacing = 2
        self.__chooser(view)

        self.navigator_callback = changed_navigator_callback
        self.navigator = None

    def set_navigator(self, storage, nbcallbacks, setstatus, mode):
        """ Common information for navigator instantiation
        :param storage: The CIM storage
        :param nbcallbacks: The callbacks used in the navigator
        :param setstatus: method for showing result messages
        :return:
        """
        self.storage = storage
        self.nbcallbacks = nbcallbacks
        self.status = setstatus
        self.mode = mode

        self.navigator = self.__set_navigator()

    def __set_navigator(self):
        """Set or reset navigator """
        if self.navigator:
            self.navigator.destroy()

        navigator = cimNBnavigator(self.storage,
                                self.subcollections[self.get_active()],
                                self.status, self.nbcallbacks, self.mode)

        self.pack_start(navigator, True, True, self.spacing)
        navigator.show_all()
        return navigator

    def __chooser(self, value='designing'):
        """ Construct the choice of sub-collections """

        box = gtk.HBox(spacing=self.spacing)
        label = gtk.Label('Collection documents for ')
        box.pack_start(label, False, False, self.spacing)

        self.entry = gtk.combo_box_new_text()
        for m in self.subcollections:
            self.entry.append_text(m)
        assert value in self.subcollections
        index = self.subcollections.keys().index(value)
        self.entry.set_active(index)
        self.entry.connect('changed', self.changed_entry)
        box.pack_start(self.entry, True, True, self.spacing)

        # Now put the entry box at the top of this frame
        self.pack_start(box, False, False, self.spacing)

    def changed_entry(self,widget=None):
        """ This is the callback from an entry change """
        print 'Active', self.get_active()
        self.navigator = self.__set_navigator()
        self.navigator_callback(self.navigator)

    def show(self):
        """ Show all the contents """
        super(collectionVBox, self).show_all()

    def get_active(self):
        '''
        :return: The currently active sub-collection
        '''
        model = self.entry.get_model()
        index = self.entry.get_active()
        return model[index][0]

    def get_collection(self):
        """Return a docset for the currently active selection"""
        return self.subcollections[self.get_active()]

class cimNBnavigator(indexedNB):

    """ Provides a Notebook Frame with all the document types available for selection
    either one by one (for edit/view) or a by selecting some (or all) of one particular
    type, for export or publication.
    """

    def __init__(self, storage, docset, status, button_callbacks, mode='EDIT/VIEW'):

        """
        Constructor for the navigation/selection function within the CIM notebook.
        :param storage: current cim storage
        :param docset: the current sub-collection set
        :param status: status method for messages
        :param button_callbacks: dictionary of callback methods for buttons on pages
        :param mode: choice from ['EDIT/VIEW','EXPORT']
        :return: None
        """

        super(cimNBnavigator, self).__init__()

        self.status = status
        self.storage = storage
        self.button_callbacks = button_callbacks

        self.build_all_lists(docset, mode)

    def __doc2list(self, doc):
        """ Find the list appropriate for this doc """

        # Hack for NumericalRequirement and ScientificDomain lists
        #      and for extension points.

        # In these cases we want to show all documents which have
        # these as base classes.

        # There are two routes in here: either we get a document handed
        # to us from the various creation tools, which may be a sub-class
        # of the base class tabs, or we have the base class tab and we
        # need to find all the sub-class documents.

        # Parse all known document types and see if they fall into
        # these categories. Need to do this every time in case
        # vocabulary choice has chagned.

        options = {'NumericalRequirement': ['NumericalRequirement'],
                   'ScientificDomain': ['ScientificDomain']}
        for o in options:
            for d in self.storage.registries:
                if o in self.storage.factory.base(d):
                    options[o].append(d)
        for p in definitions.extension_points:
            # are they currently available in the factory?
            if p in self.storage.factory.packages:
                for d in definitions.extension_points[p]:
                    options[d] = [d, definitions.extension_points[p][d]]

        all_docs = [doc, ]
        for o in options:
            if doc in options[o]:
                # if this doc is one of the collectives, use it instead
                all_docs = options[o]
                doc = o
                break

        if doc == 'Cmip6Model':
            print 'Oh No'
            print options

        return doc, all_docs

    def get_list_of_docs(self, doc):
        """ A registry suitable for display in the appropriate window"""

        doc, all_docs = self.__doc2list(doc)

        data = pr.Registry(doc)

        for d in all_docs:
            data += self.storage.data[d]

        return data, doc

    def build_list(self, docset, doc, mode):

        """ Builds the list visible in the tab """

        data, doc = self.get_list_of_docs(doc)

        if doc in docset:

            if mode == 'EDIT/VIEW':
                new_page = nbw.nbListView(
                    data, doc, mode,
                    self.button_callbacks['changer'],
                    self.button_callbacks['viewer'],
                    self.button_callbacks['adder'],
                    self.button_callbacks['grapher'])
            else:
                new_page = nbw.nbSelectView(
                    data, doc, mode,
                    self.button_callbacks['changer'],
                    self.button_callbacks['exporter'])
            # NB: We have to do the show before the append to notebook
            # otherwise we can't set to the new page. See
            # http://comments.gmane.org/gmane.comp.gnome.gtk%2B.general/19630
            new_page.show()
            if doc in self.widgets:
                # kill the old page and replace
                print 'replacing ', docset[doc]
                self.replace_page(new_page, doc, docset[doc])
            else:
                print 'adding', docset[doc]
                self.add_page(new_page, doc, docset[doc])
        else:
            print docset
            self.status('Unable to show collection %s' % doc)

    def build_all_lists(self, docset, mode, show=None):
        """ Rebuild all lists. If show, make sure the current list
        is the one for that particular document type"""
        #
        # It looks like we need to rebuild all lists when we come back from
        # any document dialog since it's possible that the link dialog has
        # been called and it's added new documents. We could avoid this by
        # tracking any changed lists all the way back up from the linked dialog
        # via cmwBasic ubercallback then entity callback to here. Meanwhile,
        # while the lists are relatively small, we can probably afford this.
        #
        for doc in docset:
            self.build_list(docset, doc, mode)

        if show:
            self.go2page(show)

    def go2page(self, key):
        """ Set current page in the notebook, given document key """
        key, all_docs = self.__doc2list(key)
        assert key in self.widgets,' key %s not in collection tab widgets ' % key
        widget_target = self.widgets[key]
        p = self.page_num(widget_target)
        self.set_current_page(p)

class cimNB:

    """ Provides an interface to CIM content by providing two major attributes:
        - the collection frame for navigation, and
        - the document frame for manipulating documents.
    These two frames are shown in panes by the cimNBcontroller """

    def __init__(self, statusbar, last_used_storage=None):

        """
        Constructor for the two panes of cimNB content.
        :param statusbar: statusbar from controller
        :param last_used_storage: location on disk of last used storage database
        :return: None
        """
        
        self.statusbar = statusbar
        self.statusID = self.statusbar.get_context_id('cimNB')
        self.last_used_storage = last_used_storage
        
        self.collectionFrame = nbw.nbMixinFrame(' Collection ')
        self.documentFrame = nbw.nbMixinFrame(' Documents ')

        self.nbcallbacks = {
            'changer':self.changer,
            'viewer':self.viewer,
            'grapher':self.grapher,
            'exporter':self.exporter,
            'adder':self.adder
        }

        # Left and right panes:
        self.collectionBox = None
        self.documentNB = None

        self.currentMode = 'EDIT/VIEW'

        self.startup(True)
        
    def setstatus(self,message):
        """ Convenience method for pushing status messages """
        self.statusbar.push(self.statusID, message)
        print 'Status ',message
        
    def startup(self, reload=False, newfile=None, view='designing', sheet=False):
        """ Load (or reload) a new collection and if necessary repaint the notebooks.
        This is currently called by the cimNBcontroller on instantiation
        and when the collection changes."""

        if newfile is not None:
            self.last_used_storage = newfile

        if self.collectionBox:
            self.collectionFrame.remove(self.collectionBox)
            del self.collectionBox
        self.collectionBox = collectionVBox(self._handle_navigator, view)

        if reload or sheet:
            # We've got a new collection to work with
            if reload:
                self._load_storage()
                # pop up a message from the storage
                self.setstatus(self.cim.status)
            self.collectionBox.set_navigator(
                        self.cim, self.nbcallbacks, self.setstatus, self.currentMode)
            self.collectionNB = self.collectionBox.navigator
            self.currentMode = 'EDIT/VIEW'

        self.collectionFrame.add(self.collectionBox)
        self.collectionBox.show()

        self.makeDocumentNB(reload or sheet)

    def load_spreadsheet(self, filename, view='designing'):
        docs = pss.read_spreadsheet(filename)
        for doc in docs:
            #FIXME: Deal with existing documents
            self.cim.add(doc)

        self.startup(sheet=True, view=view)
        self.setstatus('%s documents read from spreadsheet' % len(docs))

    def _handle_navigator(self,navigator):
        """This is the method for handling navigator instances and callbacks """
        del self.collectionNB  # Not sure I trust the garbage handling ...
        self.collectionNB = navigator

    def _load_storage(self):
        """Load the storage """
        if self.last_used_storage is None:
            self.cim = cimStorage()
        else:
            self.cim = cimStorage(self.last_used_storage)
            
    def close_down(self):
        f = open(last_used_file, 'w')
        f.write(self.cim.dirname)
        f.close()          
            
    def save(self, filename=None):
        """ Save current collection to disk """
        if filename is not None:
            self.cim.dirname = filename
        self.cim.write()
        self.last_used_storage = self.cim.dirname
        self.statusbar.push(self.statusID, self.cim.status)
        
    def changer(self,data):
        """ Takes a callback from the collection page to choose
        between the single and multi view """
        assert data[0] in ['EXPORT','EDIT/VIEW']
        if data[0] <> self.currentMode:
            self.currentMode = data
            self.collectionNB.build_list(data[1], data[0])
        
    def exporter(self,data):
        """ Placeholder for export functionality """
        dtable=pcsv.set2table(data)
        dname=data[0].cimType
        chooser = gtk.FileChooserDialog(title='Save csv',
                        action=gtk.FILE_CHOOSER_ACTION_SAVE,
                        buttons=(gtk.STOCK_CANCEL,gtk.RESPONSE_CANCEL,
                        gtk.STOCK_SAVE,gtk.RESPONSE_OK)
                        )
        chooser.set_current_name('%s.csv'%dname)
        response = chooser.run()
        if response == gtk.RESPONSE_OK:
            newfile=chooser.get_filename()
            dtable.write(newfile)
            self.setstatus('Saved %s'%newfile)
        elif response == gtk.RESPONSE_CANCEL:
            self.setstatus('Cancelled. No file selected, nothing saved')
        chooser.destroy()

    def makeDocumentNB(self,replace):
        """ Make a notebook for displaying documents """
        
        if self.documentNB:
            self.documentFrame.remove(self.documentNB)
            del self.documentNB
        
        self.documentNB = indexedNB()

        self.documentFrame.add(self.documentNB)
        
        if replace:
            self.documentNB.show_all()
    
    def adder(self,context,cimType):
        """ Provides a tabbed pageimport mp.core.schema.cmip6 as cmip6 for adding entities according to cimType.
        This is used as a callback function from the list view add button """
        title = 'New %s' % cimType
        e = factory.make_cim_instance(cimType, self.cim.factory)
        e.meta.metadata_author = self.getDefaultAuthor()
        widget = vw.cimwBasic(e, store=self.cim)
        widget.connect('clicked', self.entity_callback, cimType)
        widget.show()
        self.documentNB.add_page(widget, e.uid, title)
        self.documentNB.et_current_page(-1)
            
    def viewer(self,context,entity):
        """ Provides a tabbed page for examining an entity """
        if context == 'Copy':
            entity = entity.copy()
            entity.meta.metadata_author = self.getDefaultAuthor()

        widget = vw.cimwBasic(entity, store=self.cim)
        widget.connect('clicked', self.entity_callback, context)
        widget.show()
        ftitle = '%s: %s'%(entity.cimType,entity.name)
        self.documentNB.add_page(widget, entity.uid, ftitle)
        self.documentNB.set_current_page(-1)
    
    def grapher(self,context,doc):
        triples = doc.links
        if len(triples) == 0:
            self.setstatus('No triples found in document, no plot possible')
        else:
            dialog = nbw.imageDoc(self.cim, doc, triples, self.entity_callback)
            dialog.show()

    def _setDefaultMeta(self):
        """ Sets the default metadata author for changes using the GUI"""
        metaauth = self.cim.factory.build('Party', initialise_metadata=True)
        self.md = vw.cimwDialog('Party', metaauth, 'Party',
                self.__metaCallback, 'Please provide default metadata author', self.cim, True)
        self.md.run()
        self.md.destroy()

    def getDefaultAuthor(self):
        """ Setup default metadata """
        if self.cim.default_metadata_author is None:
            self._setDefaultMeta()
            # at this point we hope the callback has set the default
            if self.cim.default_metadata_author is None:
                #ie *still* none, user has cancelled or otherwise blown the dialog
                self.setstatus('No default metadata author provided')
        return self.cim.default_metadata_author
    
    def __metaCallback(self,widget,data):
        """ Takes the callback for default metadata """
        value, origin = data
        self.cim.set_default_author(value)
        self.md.destroy()
        
    def entity_callback(self, widget, data):
        """ Update as necessary """
        # Note that the connect in viewer and adder goes via
        # an ubercallback that adds the origin to the data to give
        # the data tuple response we see below.
        payload, origin = data
        assert isinstance(payload, uml.umlClass)
        print 'Entity callback for %s %s is %s' % (payload.name, payload.uid, origin)

        try:
            uid = payload.uid
        except AttributeError:
            # user has deleted the uid?
            if origin != 'Quit':
                self.setstatus('Notebook status undefined if you delete the metadata uid!')
        
        reload_fixer = 0
        if origin.startswith('Cancel'):
            self.setstatus(origin)
            return
        elif origin not in ['Quit','Delete']:
            if origin.startswith('Fix//'):
                raise ValueError('This code is obsolete')
                reload_fixer = 1
            self.cim.add(payload)
            self.setstatus('Saved %s %s'%(payload.cimType, payload))
        elif origin == 'Delete':
            msgArgs = payload.cimType, payload.name
            dialog = gtk.MessageDialog(None, 0, 
                gtk.MESSAGE_QUESTION,
                gtk.BUTTONS_OK_CANCEL,
                "Are you sure you want to delete %s [%s]"%msgArgs)
            response = dialog.run()
            if response == gtk.RESPONSE_OK:
                # it might not be in the cimstore, 
                # if this was a copy that's being deleted before saving
                if uid in self.cim.data[payload.cimType]:
                    self.cim.delete(payload)
                    self.setstatus('Deleted %s [%s]'%msgArgs)
                else: 
                    self.setstatus('Loosing copy of %s [%s]'%msgArgs)
                dialog.destroy()    
            else:
                self.setstatus('Cancelled deleting %s [%s]'%msgArgs)
                dialog.destroy()
                # at this point the ubercallback will want to know if
                # it should kill itsefl ... but in this case, no:
                return False 
        if origin != 'Save': self.documentNB.remove_by_key(uid)
        docset = self.collectionBox.get_collection()
        self.collectionNB.build_all_lists(docset,'EDIT/VIEW',payload.cimType)
        if reload_fixer:
            self.documentNB.remove_by_key('Fixlinks')
            self.tools_fixlinks()
        return origin != 'Save' # ubercallback should kill itself unless a save
        
    def tools_fixlinks(self):
        """ Find and assign cimlinks within existing documents """
        links=pt.find_links_in_store(self.cim)
        widget=nbw.linkHandlerPage(
            links,self.cim,self.viewer,self.tools_callback)
        widget.show()
        ftitle='Resolve Links'
        self.documentNB.add_page(widget,'Fixlinks',ftitle)
        self.documentNB.set_current_page(-1)
        
    def tools_callback(self,data):
        """ Used as a callback from the tool pages """
        self.documentNB.remove_by_key(data)

class cimNBcontroller:
    """ Provides a controller view for manipulating CIM content, embedds
    two panels for content. Navigation and listing on the left,
    content editing and viewing on the right. Menus above, and status below."""
    
    def __init__(self,lastUsedStorage=None):
        """ Constructor for the entire notebook content. """
        
        # provide the window 
        window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        window.connect("delete_event", self.delete)
        window.set_border_width(10)
        
        # provide a paned window within, document lists on the left,
        # documents on the right.
        
        # vbox with menubar, paned window, message bar
        
        vbox=gtk.VBox()
        window.add(vbox)
        menubar=self.get_mainMenu(window)
        vbox.pack_start(menubar,expand=False)
        
        panes=gtk.HPaned()
        vbox.pack_start(panes,padding=5)
        
        # We're going to use the bottom row for event notification
        sbox=gtk.HBox()
        statusbar=gtk.Statusbar()
        statusbar.set_has_resize_grip(False)
        label=gtk.Label('Status: ')
        sbox.pack_start(label,expand=False)
        sbox.pack_start(statusbar,expand=True)
        statusbar.modify_base(gtk.STATE_NORMAL,gtk.gdk.color_parse('blue'))
        vbox.pack_start(sbox,padding=2,expand=False)

        # Now add those two crucial panes using content from the cimNB itself.
        nb=cimNB(statusbar,lastUsedStorage)
        panes.add1(nb.collectionFrame)
        panes.add2(nb.documentFrame)
        self.nb=nb
       
        # (make it possible to have image buttons, see
        # http://stackoverflow.com/questions/2188659/stock-icons-not-shown-on-buttons )
        settings = gtk.settings_get_default()
        settings.props.gtk_button_images = True
        
        window.show_all()
        self.w=window
        self.setTitle()
    
    def setTitle(self):
        self.w.set_title(
           ' CIM Notebook - %s'%os.path.abspath(self.nb.cim.dirname))
        
    def get_mainMenu(self,window):
        self.ui = """<ui>
            <menubar name="MenuBar">
                <menu action="Collection">
                    <menuitem action="New"/>
                    <menuitem action="Open"/>
                    <menuitem action="Save"/>
                    <menuitem action="Save As"/>
                    <separator/>
                    <menuitem action="Export Selection"/>
                    <menuitem action="Import Spreadsheet"/>
                    <separator/>
                    <menuitem action="Default meta author"/>
                    <menuitem action="Collection Type"/>
                    <menuitem action="Find Duplicates"/>
                    <menuitem action="Extract Links"/>
                    <separator/>
                    <menuitem action="Quit"/>
                </menu>
                <menu action="Document">
                    <menuitem action="Save Document"/>
                    <separator/>
                    <menuitem action="Delete Document"/>
                </menu>
                <menu action="Search">
                    <menuitem action="In Current Documents"/>
                    <menuitem action="In Collection"/>
                </menu>
                <menu action="Help" >
                    <menuitem action="About"/>
                    <menuitem action="Concept Documentation"/>
                </menu>
            </menubar>
            </ui>
            """
        uimanager = gtk.UIManager()

        accelgroup = uimanager.get_accel_group()
        window.add_accel_group(accelgroup)

        actiongroup = gtk.ActionGroup('cimNB')
        self.actiongroup = actiongroup

        # Create actions
        actiongroup.add_actions([ ('Collection', None, '_Collection'),
                                  ('New',gtk.STOCK_NEW,'New Collection', None,
                                  'Create new collection',self.menu_new),
                                  ('Save',gtk.STOCK_SAVE,'Save Collection', None,
                                  'Save collection to disk',self.menu_save),
                                  ('Save As',gtk.STOCK_SAVE_AS,'Save Collection As', None,
                                  'Save collection to new disk location',self.menu_save_as),
                                  ('Quit', gtk.STOCK_QUIT, 'Quit', None,
                                  'Quit without saving', self.menu_quit),
                                  ('Open',gtk.STOCK_OPEN,'Open Collection', None,
                                  'Open a collection directory',self.menu_open),
                                  ('Export Selection',None,'Export selection of documents',None,
                                   'Explain how to export a selection of documents',self.export_dialog),
                                    ('Import Spreadsheet',None,'Import a cimNB spreadsheet',None,
                                     'Import a spreadhsheet',self.menu_import_sheet),
                                  ('Default meta author',None,'Set default metadata author',None,
                                   'Set the metadata author used by default for new documents',
                                   self.menu_meta_author),
                                  ('Collection Type', None, "Set collection vocabulary", None,
                                   "Set the primary vocabulary type for the collection",
                                   self.menu_set_collection_type),
                                  ('Find Duplicates',None,'Find Duplicates',None,
                                   'Find duplicate documents by name',self.menu_find_duplicates),
                                  ('Extract Links',None,'Extract Links',None,
                                   'Parse documents for links and resolve',self.menu_extract_links),
                                  
                                  ('Document',None,'_Document'),
                                  ('Save Document',None,'Save document',None,
                                  'Save document into save collection',self.menu_notyet),
                                  ('Export Document',None,'Export document',None,
                                  'Export document as json/pdf',self.menu_notyet),
                                  ('Delete Document',gtk.STOCK_DELETE,'Delete document',None,
                                  'Delete document and remove from collection',self.menu_notyet),
                                  
                                  ('Search',None,'_Search'),
                                  ('In Current Documents',gtk.STOCK_FIND,'Search in current documents',None,
                                  'Search in documents of current type',self.menu_notyet),
                                   ('In Collection',None,'Search in collection',None,
                                  'Search in collection',self.menu_notyet),
                                  
                                  ('Help',None,'_Help'),
                                  ('About',gtk.STOCK_HELP,'About', None,
                                    'About this program',self.menu_help),
                                  ('Concept Documentation',None,'_Concept Documentation',None,
                                    'Documentation for the esdoc system',self.show_esdoc),  
                                    ])
        
        uimanager.insert_action_group(actiongroup, 0)
        uimanager.add_ui_from_string(self.ui)
        widget=uimanager.get_widget('/MenuBar')
        searchmenu = uimanager.get_widget('/MenuBar/Search')
        searchmenu.set_right_justified(True)
        helpmenu = uimanager.get_widget('/MenuBar/Help')
        helpmenu.set_right_justified(True)
        return widget
 
    def delete(self,widget,event=None):
        """ Method for terminating main controller """
        carry_on = True
        if not self.nb.cim.saved_to_disk:
            carry_on = self.check_on_save()
        if carry_on:
            self.nb.close_down()
            gtk.main_quit()
            return False
        return True

    def check_on_save(self):
        """Did you really mean to quit without saving?"""
        dialog = gtk.MessageDialog(type=gtk.MESSAGE_QUESTION, buttons=gtk.BUTTONS_YES_NO)
        dialog.set_markup("Data not saved, quit anyway?")
        response = dialog.run()
        assert response in [gtk.RESPONSE_YES,gtk.RESPONSE_NO]
        dialog.destroy()
        return {gtk.RESPONSE_NO:False,gtk.RESPONSE_YES:True}[response]

    def menu_quit(self,b):
        self.delete(b)
        
    def show_esdoc(self,b):
        """ show the esdoc concept docs """
        d=ph.helpConCIM()
        d.show()
        
    def export_dialog(self,b):
        """ Explain how to export files """
        message=gtk.MessageDialog(type=gtk.MESSAGE_INFO,buttons=gtk.BUTTONS_OK)
        txt=self.obtain_help_from('help_export.txt')
        message.set_markup(txt)
        message.set_markup
        message.run()
        message.destroy()

    def menu_import_sheet(self,b):
        """ Import a cimNB spreadsheet """
        if not self.nb.cim.saved_to_disk:
            dialog = gtk.MessageDialog(type=gtk.MESSAGE_QUESTION, buttons=gtk.BUTTONS_YES_NO)
            dialog.set_markup("Data not saved, import anyway?")
            response = dialog.run()
            assert response in [gtk.RESPONSE_YES, gtk.RESPONSE_NO]
            dialog.destroy()
            if gtk.RESPONSE_NO:
                self.menu_save_as(b)
        chooser = gtk.FileChooserDialog(title='Open collection',
                        action=gtk.FILE_CHOOSER_ACTION_OPEN,
                        buttons=(gtk.STOCK_CANCEL,gtk.RESPONSE_CANCEL,
                        gtk.STOCK_OPEN,gtk.RESPONSE_OK)
                        )
        response = chooser.run()
        if response == gtk.RESPONSE_OK:
            newfile = chooser.get_filename()
            self.nb.load_spreadsheet(newfile)

        elif response == gtk.RESPONSE_CANCEL:
            self.nb.setstatus('Open spreadsheet cancelled')
        chooser.destroy()
        
    def menu_help(self,b):
        message=gtk.AboutDialog()
        message.set_program_name('CIM Notebook')
        message.set_copyright('(c) Bryan Lawrence')
        message.set_version(__version__)
        message.set_comments("""
This is a pre-release version of a CIM notebook.
It is intended to provide functionality for describing and planning experiments, for logging simulations, but not for fully describing models.
        """)
        message.run()
        message.destroy()
        
    def menu_notyet(self,b):
        message = gtk.MessageDialog(type=gtk.MESSAGE_ERROR, buttons=gtk.BUTTONS_OK)
        message.set_markup("Functionality not yet available")
        message.run()
        message.destroy()
        
    def menu_save(self,b):
        self.nb.save()
       
    def menu_save_as(self,b):
        chooser = gtk.FileChooserDialog(title='Experiment Notebook, Save As',
                            action=gtk.FILE_CHOOSER_ACTION_CREATE_FOLDER,
                            buttons=(gtk.STOCK_CANCEL,gtk.RESPONSE_CANCEL,
                                gtk.STOCK_SAVE_AS, gtk.RESPONSE_OK)
                                )
        chooser.set_current_folder(os.path.dirname(self.nb.cim.dirname))
        response = chooser.run()
        if response == gtk.RESPONSE_OK:
            newfile=chooser.get_filename()
            self.nb.save(newfile)
            self.setTitle()
        elif response == gtk.RESPONSE_CANCEL:
            self.nb.setstatus('No file selected, nothing saved')
        chooser.destroy()
        
    def menu_new(self,b):
        chooser = gtk.FileChooserDialog(title='Experiment Notebook, Save As',
                            action=gtk.FILE_CHOOSER_ACTION_CREATE_FOLDER,
                            buttons=(gtk.STOCK_CANCEL,gtk.RESPONSE_CANCEL,
                                gtk.STOCK_NEW,gtk.RESPONSE_OK)
                                )
        response = chooser.run()
        if response == gtk.RESPONSE_OK:
            newfile=chooser.get_filename()
            if len(os.listdir(newfile))<>0:
                self.nb.setstatus(
                    'Cannot create new collection, chosen directory has content!')
            else:
                self.nb.startup(True,newfile)
                self.setTitle()
        elif response == gtk.RESPONSE_CANCEL:
            self.nb.setstatus('Attempt to create new collection cancelled')
        chooser.destroy()
        
    def menu_open(self,b):
        """ Open and load a CIM collection """
        chooser = gtk.FileChooserDialog(title='Open collection',
                        action=gtk.FILE_CHOOSER_ACTION_SELECT_FOLDER,
                        buttons=(gtk.STOCK_CANCEL,gtk.RESPONSE_CANCEL,
                        gtk.STOCK_OPEN,gtk.RESPONSE_OK)
                        )
        response = chooser.run()
        if response == gtk.RESPONSE_OK:
            newfile=chooser.get_filename()
            self.nb.startup(True,newfile)
            self.setTitle()
        elif response == gtk.RESPONSE_CANCEL:
            self.nb.setstatus('Open new collection cancelled')
        chooser.destroy()
        
    def menu_meta_author(self, b):
        self.nb._setDefaultMeta()

    def menu_set_collection_type(self, b):
        changer = nbw.VocabDialog(None, self.nb.cim)
        changer.run()
        changer.destroy()

    def menu_extract_links(self, b):
        """ Find and assign links within documents"""
        self.nb.tools_fixlinks()
        
    def menu_find_duplicates(self, b):
        self.menu_notyet(b)
        
    def obtain_help_from(self, filename):
        """ Read and return content from a filename in the help
        file directory """
        here = os.path.dirname(__file__)
        f = open(os.path.join(here,'helptext',filename),'r')
        content = f.read()
        f.close()
        return content


def main():
    """ Main loop for cimNB """
    import os
    if os.path.exists(last_used_file):
        f = open(last_used_file)
        last_used_storage = f.readlines()[0]
    else:
        last_used_storage = None
    cimNBcontroller(last_used_storage)

    gtk.main()
    return 0


if __name__ == "__main__":
    main()
