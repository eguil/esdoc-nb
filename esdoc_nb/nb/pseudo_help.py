import tempfile

import esdoc_nb.mp.core.lib.factory
import gtk

import esdoc_nb.mp.core.lib.utils
from esdoc_nb.mp import umlview as uml
from pseudo_images import imageDialog


class helpConCIM(object):
    """ Provides a help dialog for the CIM conceptual model """

    def __init__(self):
        """ Initialise by instantiating a list of packages
        in a tree view """

        self.window = gtk.Dialog("esdoc concepts")

        self.window.set_size_request(250, 500)
        self.window.connect("delete_event", self.delete_event)

        header = """
<b>Package and Class Documentation</b>
<span font_size="small">
The esdoc notebook organises information 
using the esdoc Common Information Model 
(CIM) which itself is organised into
packages and classes - which are here 
documented using the Unifed Modelling 
Language, UML.
</span>
        """
        label = gtk.Label(header)
        label.set_use_markup(True)

        self.window.vbox.pack_start(label, expand=False)

        # create and populate a tree store with the structure
        # of the python ConCIM

        self.factory = esdoc_nb.mp.core.lib.factory.cimFactory()

        self.treestore = gtk.TreeStore(str)
        packages = sorted(self.factory.packages.keys())
        for p in packages:
            piter = self.treestore.append(None, [p, ])
            classes = self.factory.packages[p]
            classes.sort()
            for c in classes:
                self.treestore.append(piter, [c, ])

        # create the TreeView using treestore
        self.treeview = gtk.TreeView(self.treestore)
        self.treeview.connect('row-activated', self.row_clicked)

        # now setup the view
        self.pkg_column = gtk.TreeViewColumn('Packages & Classes')
        self.treeview.append_column(self.pkg_column)
        self.cell = gtk.CellRendererText()
        # add the cell to the tvcolumn and allow it to expand
        self.pkg_column.pack_start(self.cell, True)
        # set the cell "text" attribute to column 0 - retrieve text
        # from that column in treestore
        self.pkg_column.add_attribute(self.cell, 'text', 0)
        # make it searchable
        self.treeview.set_search_column(0)
        # Allow sorting on the column
        self.pkg_column.set_sort_column_id(0)
        # Allow drag and drop reordering of rows
        # self.treeview.set_reorderable(True)

        self.sw = gtk.ScrolledWindow()
        self.sw.set_policy(gtk.POLICY_NEVER, gtk.POLICY_ALWAYS)
        self.sw.add(self.treeview)

        self.window.vbox.pack_start(self.sw, expand=True)

    def delete_event(self, widget, event, data=None):
        """ Close the window and quit """
        self.window.destroy()

    def row_clicked(self, treeview, path, column):
        """ This is called when a row is clicked on """
        # column is relatively uninteresting, since at the moment
        # we only have one column.
        # print column.get_title()

        model = treeview.get_model()
        iter = model.get_iter(path)
        entity = model.get_value(iter, 0)
        if entity in self.factory.classes:
            self.view(entity)

    def show(self):
        self.window.show_all()

    def view(self, entity):
        """ View and show a UML diagram and documentation for a specific class """
        cv = classView(entity)
        cv.show()


class classView(imageDialog):
    """ View information about a specific CIM class """

    def __init__(self, class_name):
        """ Initialise with a class name """
        super(classView, self).__init__('UML view of %s' % class_name)
        super(classView, self).show()

        self.dia = uml.umlDiagram()
        self.makeImage(class_name)
        self.makeDocs(class_name)

    def makeImage(self, name):
        """ Make and load the image """

        # get temporary output file
        fn, pfile = tempfile.mkstemp()

        # make the plot
        dia = self.dia
        dia.setClassDoc(name)
        dia.plot(filebase=pfile)

        # record plot files for later deleting
        self.internal_files = dia.output_files

        # load up image
        self.image = gtk.Image()
        self.image.set_from_file(dia.output_files[0])
        self.imageBox.pack_start(self.image)

        self.image.show()


    def __getproperties(self,cname):
        """
        :param cname: Class name for which we are collecting properties
        :return myproperties: list of properties from this class
        """
        # Need to collect a set of key,value pairs of property names and definitions
        # This set both comes from the class itself, and any parents.
        #
        x = esdoc_nb.mp.core.lib.factory.make_cim_instance(cname)
        return [x.attributes[a] for a in x.attributes]

    def makeDocs(self, name):
        """ Make some text docs for the properties """
        label_box = gtk.VBox()

        for p in self.__getproperties(name):
            line = gtk.HSeparator()
            label_box.pack_start(line, False, True, 2)
            line.show()
            content = '<b>%s</b>: %s' % (p.name, p.doc)
            label = gtk.Label(content)
            label.set_use_markup(True)
            label_box.pack_start(label, expand=False)
            label.set_alignment(0, 0)
            label.set_width_chars(40)
            label.set_line_wrap(True)
            label.show()
        self.caption.pack_start(label_box)
        label_box.show()


if __name__ == "__main__":
    h = helpConCIM()
    h.show()
    gtk.main()
