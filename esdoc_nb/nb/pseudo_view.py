##
## provides view widgets into the property classes in psuedo_mp
##
import unittest
from datetime import datetime

import gtk

import pseudo_help as ph
import pseudo_utils as pu
import pseudo_widgets as nw
import pseudo_storage as ps
import tempfile, shutil, os
from collections import OrderedDict

import esdoc_nb.mp.core.lib.factory as factory
import esdoc_nb.mp.core.lib.uml as uml
import esdoc_nb.mp.core.lib.definitions as definitions

test_cmip6 = 0
if test_cmip6:
    import esdoc_nb.mp.core.schema.cmip6 as cmip6


# It is such a hassle getting widget sizes right in pygtk, that we implement our own
# These control the maximum size of the widget entry dialogs by letting us construct
# something approximate.
DEFAULT_WIDGET_HEIGHT = 35
TEXT_WIDGET_HEIGHT = 150
WIDGET_OFFSET = 140  # buttons, frame title etc

#
# Section One: Basic Widgets ***************************************************
#

class strSingle(gtk.HBox):
    ''' Provides an entry box for a single string '''
    def __init__(self,thisProperty,parent_callback,entryLength=0,
                labelLength=-1):
        ''' Provides an entry frame for a single valued CIM string property.
        Pass the property as a tuple of key name and currentvalue,
        and a callback which can take the property name and value.
        Optionally pass the required length of the entry and/or the label.
        Be careful with entrylength, it looks like copy and paste is silenty
        truncated beyond entrylength ... zero is no limit.
        '''
        super(strSingle, self).__init__(spacing=5)
        propertyName, inValue, typ, doc= thisProperty
        self.label = gtk.Label(propertyName)
        tooltip = gtk.Tooltips()
        tooltip.set_tip(self.label, doc)
        self.widget_height = DEFAULT_WIDGET_HEIGHT

        if labelLength != -1:
            self.label.set_width_chars(labelLength)
        self.pack_start(self.label, False, False, 5)
        self.callback = parent_callback
        self.textEntry = gtk.Entry()
        if inValue is not None:
            self.setv(str(inValue))
        else:
            self.value = None
        self.textEntry.set_max_length(entryLength)
        self.textEntry.connect("activate", self.mycallback,
                (self.textEntry, propertyName))
        self.pack_start(self.textEntry, True, True, 5)
        self.multi = 0
        
        # next button only shown in a multicontext, and the show
        # and connect are handled externally.
        self.moreButton=pu.ibutton(gtk.STOCK_ADD, '')
        self.pack_start(self.moreButton, expand=False, padding=5)
        
        # this next button always shown unless sub-classed
        self.dbutton=pu.ibutton(gtk.STOCK_CLEAR,'')
        self.dbutton.connect('clicked', self.deleteMe, (self.textEntry, propertyName))
        self.pack_start(self.dbutton, expand=False, padding=5)
        
    def deleteMe(self,w,data):
        self.textEntry.set_text('')
        self.mycallback(self,data)
        
    def mycallback(self,widget,data):
        ''' Internal callback to get property values '''
        entry,propertyName=data
        value=entry.get_text()
        self.value=value
        self.callback(propertyName,value)
        
    def activate(self):
        ''' Activate the text entry to update values via callback'''
        before=self.value
        self.textEntry.activate()
        after=self.value
       
    def show(self):
        self.label.show()
        self.dbutton.show()
        self.textEntry.show()
        super(strSingle,self).show()
        
    def setv(self,value):
        if value <> None:self.textEntry.set_text(value)
        self.value=value

class DisplayWidget(strSingle):
    """ Used to display attributes which cannot be changed"""
    def __init__(self,p, parent_callback, entryLength=72, labelLength=-1):
        p_old_api = p.name, p.value, p.target, p.doc
        super(DisplayWidget, self).__init__(p_old_api, parent_callback, entryLength, labelLength)
        self.textEntry.set_editable(False)
        self.textEntry.modify_base(gtk.STATE_NORMAL,gtk.gdk.color_parse('#E7E7E9'))

    def deleteMe(self, w, data):
        # No deletions allowed
        pass

    def activate(self):
        # No activations allowed
        pass

    def show(self):
        """Hide the clean button"""
        self.label.show()
        self.textEntry.show()
        super(strSingle,self).show()

class strMultiBox(gtk.VBox):
    ''' Container for a list of cim properties '''
    def __init__(self, thisProperty , parent_callback, entryLength=72, labelLength=-1):
        """ Constructor for cim property box using a umlProperty p"""
        super(strMultiBox, self).__init__(self)
        self.parent_callback = parent_callback
        self.widgets = []
        self.shown = False # used so that we only repaint if already shown
        self.elen, self.llen = entryLength, labelLength

        self.thisProperty = thisProperty
        self.propertyName = thisProperty[0]
        self.currentValue = thisProperty[1]

        self.widget_height = DEFAULT_WIDGET_HEIGHT

        self._buildmywidgets()

    def _buildmywidgets(self):
        """ Completely build or rebuild my widgets """
        p,v,t,d = self.thisProperty
        print 'Building', self.currentValue
        if self.shown:
            for w in self.widgets: w.destroy()
            self.widgets = []
        for vv in self.currentValue:
            self._setmywidget(p, vv, t, d)
        if len(self.currentValue) == 0:
            self._setmywidget(p, None, t, d)
            self.moreButton = None
        else:
            self._addlast()
        self.shown = True
        
    def _setmywidget(self, p, v, t, d):
        w = strSingle((p, v, t, d), self.mycallback,
                        entryLength=self.elen,
                        labelLength=self.llen)
        self.pack_start(w, padding=2)
        self.widgets.append(w)
        self.widget_height += w.widget_height
        return w
        
    def _addlast(self):
        """Decorate last entry with the add button and connect to method to add another one"""
        w = self.widgets[-1]
        w.moreButton.connect('clicked', self.addExtra, None)
        self.moreButton = w.moreButton
        tt = gtk.Tooltips()
        tt.set_tip(w.moreButton, 'Add an additional %s' % self.thisProperty[0])
        
    def addExtra(self,widget,data):
        ''' Called by more button to add a new value '''
        p,v,t,d=self.thisProperty
        cp=self._setmywidget(p,None,t,d)
        if self.shown: cp.show()
        self.moreButton.hide()
        
    def setv(self, value):
        if isinstance(value, list):
            raise ValueError('Need to add this code')
        else:
            self.widgets[0].setv(value)
            
    def mycallback(self,p,d):
        self._boxcallback()
            
    def _boxcallback(self):
        """Trigger parent callback with all the widget values."""
        results = [w.value for w in self.widgets]
        print results
        while None in results: results.remove(None)
        while '' in results: results.remove('')
        print results
        self.currentValue = results
        self.parent_callback(self.propertyName, results)
        self._buildmywidgets()
        for w in self.widgets: w.show()
        if self.moreButton is not None: self.moreButton.show()
      
    def activate(self):
        ''' Trigger sub widgets '''
        for w in self.widgets:w.activate()
        self._boxcallback()
        
    def show(self):
        self.shown = True
        for w in self.widgets: w.show()
        if self.moreButton is not None: self.moreButton.show()
        super(gtk.VBox, self).show()


class cimMultiBox(strMultiBox):
    """ Container for a list of cimProperties """
    def __init__(self, prop, parent_callback, idstring, store):
        """
        :param prop: a uml property of a cim class instance
        :param parent_callback: to be called when the property changes
        :param idstring: a full path into the property
        :param store: the CIM store
        :return:
        """
        self.idstring = idstring
        self.store =  store
        p_old_api = (prop.name, prop.value, prop.target, prop.doc)
        super(cimMultiBox, self).__init__(p_old_api, parent_callback)
       
    def _setmywidget(self, p, v, t, d ):
        w=cimProperty((p, v, t, d), self.mycallback, self.idstring, self.store)
        self.widgets.append(w)
        self.pack_start(w, padding=2)
        return w
    
    def addExtra(self,widget,data):
        ''' Called by more button to add a new value (overrides
        the method of the same name in the strMulti base class)'''
        p,v,t,d=self.thisProperty
        self.d=cimwDialog(p,None,t,self.extraCallback,self.idstring,self.store)
        response = self.d.run()
        # All responses are currently versions of close and forget me ... 
        self.d.destroy()
        
    def extraCallback(self,widget,data):
        ''' Takes the dialog save, and adds to the widget '''
        p,v,t,d=self.thisProperty
        self.widgets[-1].moreButton.hide()
        # I probably don't have to unconnect it as well
        w = self._setmywidget(p,data[0],t,d)
        self.d.destroy()
        w.show()
        self._addlast()
        self.moreButton.show()
        return True

class intSingle(strSingle):
    ''' Provide an entrybox for a single integer value '''
    def mycallback(self,widget,data):
        ''' Internal callback to get property values '''
        entry,propertyName=data
        value=entry.get_text()
        if value == '': return None
        try:
            ivalue=int(value)
            self.callback(propertyName,ivalue)
            self.value=ivalue
        except Exception,e:
            entry.set_text('%s (Integer!)'%self.value)
            
class floatSingle(strSingle):
    ''' Provide an entrybox for a single float value '''
    def mycallback(self,widget,data):
        ''' Internal callback to get property values '''
        entry,propertyName=data
        value=entry.get_text()
        if value =='': return None
        try:
            fvalue=float(value)
            self.callback(propertyName,fvalue)
            self.value=fvalue
        except Exception,e:
            entry.set_text('%s (Float!)'%self.value)
            
class boolSingle(strSingle):
    stringify = {0:'False',1:'True'}
    """Provide an entrybox for a single float value"""
    def setv(self, value):
        if value != None:
            self.textEntry.set_text(self.stringify[value])
        self.value = value

    def mycallback(self,widget,data):
        ''' Internal callback to get property values '''
        entry, propertyName = data
        value = entry.get_text()
        options = []

        if value =='': return None
        bvalue = bool(value)
        self.callback(propertyName, bvalue)
        self.setv(bvalue)


class intMulti(strSingle):
    ''' Provides an entrybox for multiple integer values '''
    def __init__(self,thisProperty,parent_callback,entryLength=72,labelLength=-1):
        name,value,typ,doc=thisProperty
        newvalue=','.join(value)
        super(intMulti,self).__init__((name,value,typ,doc),parent_callback,
                            entryLength=72,labelLength=-1)
    def mycallback(self,widget,data):
        ''' Internal callback to get property values '''
        entry,propertyName=data
        value=entry.get_text()
        rvalue=value.split(',')
        ivalue=[]
        try:
            ivalue=[int(r) for r in rvalue]
            self.currentValue=ivalue
        except Exception,e:
            entry.set_text('%s (Integers!)'%self.currentValue)
        self.callback(propertyName,ivalue)
        
class txtSingle(gtk.HBox):        
    ''' Provides a frame for text entry'''
    def __init__(self,thisProperty,parent_callback,
                labelLength=-1):
        ''' Provides a text entry for a single valued CIM string property.
        Pass the property as a tuple of key name and currentvalue, 
        and a callback which can take the property name and value. 
        Optionally pass the required length of the entry and/or the label
        '''
        super(txtSingle,self).__init__(spacing=10)
        self.set_size_request(250, 150)
        propertyName,inValue,typ,doc=thisProperty
        self.label=gtk.Label(propertyName)
        tooltip = gtk.Tooltips()
        tooltip.set_tip(self.label,doc)
        self.widget_height = TEXT_WIDGET_HEIGHT

        if labelLength<>-1: self.label.set_width_chars(labelLength)
        self.pack_start(self.label,False,False,5)
        self.callback=parent_callback
        self.propertyName=propertyName
        
        #need a scrolled window
        self.sw=gtk.ScrolledWindow()
        self.sw.set_shadow_type(gtk.SHADOW_ETCHED_IN)
        self.sw.set_policy(gtk.POLICY_NEVER,gtk.POLICY_AUTOMATIC)
        self.textEntry=gtk.TextView()
        self.textEntry.set_editable=(True)
        self.textEntry.set_wrap_mode(gtk.WRAP_WORD)
        self.sw.add(self.textEntry)
        
        if inValue<>None: self.set_text(str(inValue))

        self.pack_start(self.sw,True,True,5)
        self.currentValue=inValue
        self.multi=0
        
    def set_text(self,value):
        ''' Set text in widget'''
        buffer=self.textEntry.get_buffer()
        buffer.set_text(value)
        
    def get_text(self):
        ''' Return value of text in buffer'''
        buffer=self.textEntry.get_buffer()
        return buffer.get_text(*buffer.get_bounds())
        
    def mycallback(self):
        ''' Internal callback to get property values '''
        value=self.get_text()
        self.callback(self.propertyName,value)
        self.currentValue=value
        
    def activate(self):
        ''' Activate the text entry to update values via callback'''
        before=self.currentValue
        self.mycallback()
        after=self.currentValue
        #print before,after
        
    def show(self):
        self.label.show()
        self.sw.show()
        self.textEntry.show()
        super(txtSingle,self).show()

#
# Section Two: CIM Widgets *********************************************
#
class cimAlternative(gtk.VBox):
    """Provides a widget for displaying and converting alternatives"""
    def __init__(self, cimInstance, callback, idstring, store, spacing = 5):
        """
        :param cimInstance: A cim instance which we may wish to convert
        :param callback: To be called if we change the CIM instance
        :param idstring: A label for the relevant dialog
        :param store: Our database store
        :return:
        """

        self.doc = cimInstance
        self.idstring = idstring
        self.store = store
        self.spacing = spacing
        self.reject_label = None
        self.callback = callback

        super(cimAlternative, self).__init__(spacing)

        self.pack_start(gtk.HSeparator(), True, False, 2)

        self.hbox = gtk.HBox(spacing)
        self.pack_start(self.hbox, True, False, 2)

        label = gtk.Label('Alternative Formulations:')
        self.hbox.pack_start(label, False, False, spacing)

        self.entry = gtk.combo_box_entry_new_text()
        mlen = 0
        for a in cimInstance.alternatives:
            self.entry.append_text(a)
            if len(a) > mlen: mlen=len(a)
        self.entry.set_active(0)
        self.chosen = cimInstance.alternatives[0]
        self.entry.connect('changed',self.toggleAlternativeTarget)
        e = self.entry.get_child()
        e.set_width_chars(mlen)
        self.hbox.pack_start(self.entry, True, True, spacing)

        button = pu.ibutton(gtk.STOCK_OK,'Convert','Convert current document to this document type')
        self.hbox.pack_start(button, False, False, spacing)
        button.connect('clicked',self.convert)

    def show(self):
        self.show_all()

    def toggleAlternativeTarget(self,w):
        """Callback for changes in combobox entry """
        model = self.entry.get_model()
        index = self.entry.get_active()
        self.chosen = model[index][0]

    def convert(self,w):
        """Attempt to convert existing requirement into alternative formulation"""
        new = factory.make_cim_instance(self.chosen)
        meta = self.doc.meta
        if self.reject_label:
            self.reject_label.destroy()
        reject = ''
        for a in self.doc.attributes:
            existing = self.doc.attributes[a]
            if existing.value:
                # we can only do the conversion if <a> exists in the new document
                if a in new.attributes:
                    setattr(new,a,existing.value)
                else:
                    reject+=a+';'
        print 'rejection status', reject
        if reject:
            self.reject_label = gtk.Label('Cannot convert until the following attributes are removed: %s' % reject)
            self.reject_label.set_line_wrap(True)
            self.pack_start(self.reject_label)
            self.reject_label.show()
        else:
            new.meta = meta
            # At this point, the magic is that we want to change the old document.
            # This is basically nasty.
            # Start by saving it as it is (in case it doesn't exist yet)
            # Then delete it, and we eventually add the new document
            self.store.add(self.doc)
            self.store.delete(self.doc)
            self.doc = new
            self.callback(self, new)


class cimwDialog(object):
    """ A cim class widget dialog """
    def __init__ (self, propertyName, value, typ, parent_callback, idstring, store, nolink=False):
        """ Initialise with property name, value and type, and
        a callback for the widget save button.
        If nolink is True, override the attempt to construct a link, and allow a proper entry
        of a full document """
        self.parent_callback = parent_callback
        self.store = store
        if value is None:
            v = factory.make_cim_instance(typ)
            if not nolink and self.store.factory.isdoc(typ):
                # In a dialog, we're making a link to a remote document
                v = factory.make_cim_instance('DocReference')
                v.remote_type = typ
        else:
            v = value
            
        self.setWidget(v)
        self.dialog = gtk.Dialog(title=idstring)

        self.dialog.set_border_width(5)
        
        # alternative box
        self.awidgets=[]
        if v.alternatives<>[]:
            
            self.aframe=gtk.Frame('Alternatives to %s'%v.cimType)
            self.aframe.set_shadow_type(gtk.SHADOW_IN)
            self.aframe.set_border_width(5)
            self.awidgets.append(self.aframe)
            avbox=gtk.VBox()
            self.awidgets.append(avbox)
            self.aframe.add(avbox)
            
            if len(v.alternatives)==1:
                nas='This'
            else: nas='One of these'
            doclabel=gtk.Label(
                ' %s can be used instead of <i>%s</i> for <i>%s</i> '%
                (nas,v.cimType,propertyName))
            doclabel.set_use_markup(True)
            self.awidgets.append(doclabel)
            
            # need enough hboxes to hold the alternatives. Assume
            # we can have three per line, and we need one for the label
            nperline=3
            nhboxes=len(v.alternatives)/nperline+2
            aboxes=[gtk.HBox() for i in range(nhboxes)]    
            for box in aboxes:
                avbox.pack_start(box,padding=2,expand=False)
                self.awidgets.append(box)
                
            aboxes[0].pack_start(doclabel)    
            iv=0
            for i in v.alternatives:
                button = gtk.Button()
                button.set_label(i)
                button.connect('clicked',self.gotoAlternative,i)
                boxi=1+iv/nperline
                aboxes[boxi].pack_start(button,padding=5,expand=False)
                self.awidgets.append(button)
                iv+=1
           
            self.dialog.vbox.pack_start(self.aframe,expand=False,padding=5)

        self.dialog.vbox.pack_start(self.widget,padding=5)
        
        self.dialog.add_buttons(gtk.STOCK_CLOSE,1,gtk.STOCK_CANCEL,2)
        
        self.widget.show()

        # I have no idea how to get pygtk to get this right automagically,
        # so this is an attempt to get it roughly right.
        xsize = 450
        ysize = min(WIDGET_OFFSET+self.widget.widget_height,700)
        self.dialog.set_size_request(xsize,ysize)

        for a in self.awidgets:
            a.show()
            print a.size_request()

    
    def setWidget(self,v):
        ''' Set the primary entry widget '''
        neww=cimwBasic(v,self.store)
        neww.connect('clicked',self.parent_callback,'dialog')
        if hasattr(self,'widget'): 
            self.dialog.vbox.remove(self.widget)
            self.dialog.vbox.remove(self.aframe)
            self.dialog.vbox.pack_start(neww,padding=5)
        self.widget=neww
    
    def run(self):
        return self.dialog.run()
        
    def gotoAlternative(self,widget,data):
        ''' Change the internal widget to a different compatible alternative'''
        v= factory.make_cim_instance(data)
        self.setWidget(v)
        self.widget.show()
    
    def destroy(self):
        self.dialog.destroy()

class cimProperty(gtk.HBox):
    ''' Provides a pop up entry dialog to handle single valued embedded
    CIM properties '''
    def __init__(self, thisProperty, parent_callback, idstring, store):
        """ Create cimProperty widget for umlProperty p"""
        super(gtk.HBox,self).__init__()
        self.store=store
        self.propertyName,self.value,self.typ,self.doc=thisProperty
        self.parent_callback=parent_callback
        self.idstring=idstring
        
        self.label=gtk.Label('%s\n<span size="x-small">(%s)</span>'%(self.propertyName,self.typ))
        self.label.set_use_markup(True)
        self.pack_start(self.label,expand=False,padding=5)
        self.widget_height = DEFAULT_WIDGET_HEIGHT
        tooltip = gtk.Tooltips()
        tooltip.set_tip(self.label,self.doc)
        
        choices = None
        self.choices = choices
        
        # we use eframe to be a place holder for the entry
        # which we will occasionally need to kill and replace.
        self.eframe=gtk.Frame()

        if choices:
            self._setupcombo()
            self.eframe.set_shadow_type(gtk.SHADOW_NONE)
        else:
            if self.typ != 'cimtext':
                self.entry = gtk.Entry()
                self.setv(self.value)
                self.entry.set_editable(False)
                self.eframe.set_shadow_type(gtk.SHADOW_NONE)
                self.entry.modify_base(gtk.STATE_NORMAL,
                                   gtk.gdk.color_parse('#E7E7E9'))
                self.eframe.add(self.entry)
            else:
                # need an eventbox to color the label ...
                # http://faq.pygtk.org/index.py?req=show&file=faq04.016.htp
                ebox = gtk.EventBox()
                self.entry = gtk.Label()
                self.setv(self.value)
                self.entry.set_line_wrap(True)
                ebox.add(self.entry)
                ebox.show()
                ebox.modify_bg(gtk.STATE_NORMAL,
                                   gtk.gdk.color_parse('#E7E7E9'))
                self.eframe.add(ebox)
                self.eframe.set_shadow_type(gtk.SHADOW_IN)
        
        self.pack_start(self.eframe,padding=5)
    
        # next button only shown in a multicontext, and the show
        # and connect are handled externally.
        self.moreButton=pu.ibutton(gtk.STOCK_ADD,'')
        self.pack_start(self.moreButton,expand=False,padding=5)
        
        # next two buttons always shown.
        self.button=pu.ibutton(gtk.STOCK_EDIT,'')
        self.button.connect('clicked',self.makeDialog)
        self.pack_start(self.button,expand=False,padding=5)

        # except if it is a metadata record, we don't allow it to be cleaned
        if self.typ!='meta':
            self.dbutton=pu.ibutton(gtk.STOCK_CLEAR,'')
            self.dbutton.connect('clicked',self.deleteMe)
            self.pack_start(self.dbutton,expand=False,padding=5)
        else: self.dbutton=None
        
    def _setupcombo(self,reset=False):
        ''' Set up the combo entry '''
        if reset: self.entry.destroy()
        self.entry=gtk.combo_box_new_text()
        #this next would occur if someone has set something but it has yet to be saved
        if self.value is not None and str(self.value) not in self.choices:
            self.choices.append(str(self.value))
            self.store.listable_entities[self.typ.append(self.value)]
        for c in self.choices:
            self.entry.append_text(c)
        if self.value is None:
            index=0
        else:
            index=self.choices.index(str(self.value))
        self.entry.set_active(index)
        self.entry.connect("changed", self.myComboCallback)
        self.eframe.add(self.entry)
        if reset: 
            self.entry.show()
            
    def deleteMe(self,w):
        self.mycallback(self,(None,'deleteMe'))
        
    def makeDialog(self,w):
        ''' Make the popup dialog '''
        self.d=cimwDialog(self.propertyName,self.value,self.typ,
            self.mycallback,self.idstring,self.store)
        response=self.d.run()
        # All responses are currently versions of close and forget me ... 
        self.d.destroy()
        
    def mycallback(self,widget,data):
        """ This is the callback we give to the dialog to set values here"""
        value, origin = data
        # Next option occurs if this is a delete button call from a dialog property
        if origin == 'Delete': value = None
        # FIXME To be sure I understand all these delete options
        # This is from my own deleteMe call ...
        if origin != 'deleteMe':  self.d.dialog.destroy()
        if self.choices:
            self.value = value
            self._setupcombo(True)
        else:
            self.setv(value)
        self.parent_callback(self.propertyName, self.value)
    
    def activate(self):
        ''' Trigger loading from the dialog widget '''
        # not needed here, we set it when anything happens
        pass
        
    def myComboCallback(self,widget):
        '''Called if the combo box - if present - is changed '''
        model=self.entry.get_model()
        index=self.entry.get_active()
        self.value=self.store.listable_entities[self.typ][index]
        self.parent_callback(self.propertyName,self.value)
        # we don't need to repaint the combo if it's just
        # a standard combo box change of choice ...
        
    def setv(self, value):
        """Set variable displayed in entry (not combobox)"""
        self.value = value
        self.entry.set_text(str(value))
        
    def show(self):
        self.label.show()
        self.eframe.show()
        self.entry.show()
        self.button.show()
        if self.dbutton: self.dbutton.show()
        super(cimProperty,self).show()


class MultiEnumWidget(gtk.HBox):
    """ Provides a widget for selecting multiple values from an Enum """
    def __init__(self, umlp, store, callback, labelLength=-1):
        """
        Instantiates the multiple CIM Enum widget.
        :param umlp: The property which this widget is selecting
        :param store: The existing cim storage instance (including factory)
        :param callback: An external callback to update when values in this widget are changed
        :param labelLength: Controls the length of the label
        :return:
        """
        assert not umlp.single_valued

        spacing = 5
        self.umlp = umlp
        self.store = store
        self.callback = callback
        self.enum = store.factory.build(self.umlp.target)
        self.widget_height = DEFAULT_WIDGET_HEIGHT

        super(MultiEnumWidget, self).__init__(spacing=spacing)

        label = gtk.Label(umlp.name)
        if labelLength != -1:
            label.set_width_chars(labelLength)
        self.pack_start(label, False, False, spacing)

        self.pack_start(gtk.VSeparator(), True, False, 0)
        self.vbox = gtk.VBox()
        self.pack_start(self.vbox, True, True, spacing)
        self.buttons = OrderedDict()
        self._build_widgets(umlp.value)

        delete_button = pu.ibutton(gtk.STOCK_CLEAR,'')
        delete_button.connect('clicked', self._clear)

        self.pack_start(gtk.VSeparator(), True, False, 0)
        self.pack_start(delete_button, expand=False, padding=5)

    def _build_widgets(self, values):
        """ Build all the widgets"""
        print 'Multienum building for incoming value',[str(v) for v in values]
        for m in self.enum.members:
            b = gtk.CheckButton(m)
            self.buttons[m] = b
        for b in self.buttons:
            if b in [str(v) for v in values]:
                self.buttons[b].set_active(True)
        for b in self.buttons:
            self.vbox.pack_start(self.buttons[b], padding=2, expand=False)
        self.__pay_attention = True

    def activate(self):
        ''' Trigger loading from the widget '''
        value = self.__get_all_values()
        print 'multienumwidget returns', value
        self.callback(self.umlp.name, value)

    def show(self):
        self.show_all()

    def setv(self, values):
        """ Set the values within the multiple enums"""
        assert isinstance(values, list)
        for b in self.buttons:
            if b in values:
                self.buttons[b].set_active(True)
            else:
                self.buttons[b].set_active(False)

    def __get_all_values(self):
        values = []
        for b in self.buttons:
            if self.buttons[b].get_active():
                ins = self.store.factory.build(self.umlp.target)
                ins.set(b)
                values.append(ins)
        return values

    def _clear(self, *args):
        """ Clear the checkboxes in one go """
        for b in self.buttons:
            self.buttons[b].set_active(False)


class cimEnumWidget(gtk.HBox):
    """Provides a combobox choice of a CIM enum"""
    def __init__(self, umlp, store, callback, labelLength=-1):
        super(cimEnumWidget, self).__init__(spacing=5)
        self.enum = store.factory.build(umlp.target)
        self.label = gtk.Label(umlp.name)
        if labelLength != -1:
            self.label.set_width_chars(labelLength)
        self.pack_start(self.label, False, False, 5)
        self.callback = callback
        self.widget_height = DEFAULT_WIDGET_HEIGHT

        self.entry = gtk.combo_box_new_text()
        for m in self.enum.members:
            self.entry.append_text(m)
        if umlp.value is None:
            index = 0
        else:
            try:
                index = self.enum.members.keys().index(str(umlp.value))
            except ValueError, e:
                raise ValueError(str(e)+umlp.value)
        self.entry.set_active(index)
    
        self.entry.connect("changed", self.mycallback)

        self.pack_start(self.entry, True, True, 5)
        self.currentValue = umlp.value
        self.propertyName = umlp.name
        
    def mycallback(self,widget):
        ''' Internal callback to get property values '''
        model = self.entry.get_model()
        index = self.entry.get_active()
        self.enum.set(model[index][0])
        
        self.callback(self.propertyName, self.enum)
   
    def activate(self):
        ''' Trigger loading from the widget '''
        self.mycallback(self)
    
    def show(self):
        self.label.show()
        self.entry.show()
        super(cimEnumWidget, self).show()
        
    def setv(self,value):
        try:
            v = value.value
            index= self.enum.members.keys().index(v)
            self.entry.set_active(index)
        except Exception,e:
            print 'Going wrong in cimEnumWidget',value,type(value)
            print value.cimType
            print value.value
            raise Exception(e)

class cimwBasic(gtk.Frame):
    ''' Provides a basic CIM document widget frame '''
    def __init__(self, cim_instance, store):
        """
        Construct a basic interface to collect CIM properties for a CIM class instance
        :param cim_instance: a particular instance of a cimClass
        :param store: the local store
        :return: a gtk widget
        """

        assert isinstance(cim_instance, uml.umlClass), 'Widget only supports class instances'

        self.cimDoc = cim_instance
        self.store = store

        # Set up all the framing, at the end of this, we pack
        # widgets into self.vbox.
        spacing = 5
        super(cimwBasic, self).__init__()
        self.set_label(self.cimDoc.cimType)
        self.vbox = gtk.VBox()
        self.add(self.vbox)
        self.sw = gtk.ScrolledWindow()
        self.sw.set_policy(gtk.POLICY_NEVER, gtk.POLICY_AUTOMATIC)
        self.sw.set_shadow_type(gtk.SHADOW_ETCHED_IN)
        self.sw.set_border_width(spacing)
        self.inner = gtk.VBox()
        self.sw.add_with_viewport(self.inner)
        self.vbox.pack_start(self.sw, padding=2)
        self.factory = self.store.factory
        self.widget_height = 0  # manually guess this, pygtk is just too hard!

        # Now collect the widgets within the basic frame

        self.widgets = {}

        for attr in self.cimDoc.attributes:
            p = self.cimDoc.attributes[attr]
            p_old_api = p.name, p.value, p.target, p.doc
            print 'Instantiating cimwbasic widget for %s attribute %s' % (
                    self.cimDoc.cimType,p.target)
            if p.name in ['uid','metadata_last_updated','document_version'] and\
                            self.cimDoc.cimType in ['meta','minimalMeta']:
                w = DisplayWidget(p, self.__callback)
            elif p.target in self.factory.enums:
                if p.single_valued:
                    w = cimEnumWidget(p, self.store, self.__callback)
                else:
                    w = MultiEnumWidget(p, self.store, self.__callback)
            elif p.target in self.factory.classes:
                idstring = '%s/%s/%s'%(str(self.cimDoc),p.target, p.name)
                print p, p.islink
                if p.islink:
                    w = nw.LinkEntryBox(p, self.store, self.__callback, idstring)
                else:
                    if p.single_valued:
                        w = cimProperty(p_old_api, self.__callback, idstring, self.store)
                    else:
                        w = cimMultiBox(p, self.__callback, idstring, self.store)
            else:
                widget = basicWidgets[p.target][p.cardinality]
                # can't use isinstance yet, because we haven't instantiated it.
                if widget.__name__ == "BooleanEntry":
                    w = widget(p, self.__callback)
                else:
                    w = widget(p_old_api, self.__callback)
            self.widgets[p.name] = w
            self.widget_height += w.widget_height
            self.inner.pack_start(w, expand=False, padding=2)

        # If alternatives, we need to add something for those
        if self.cimDoc.isDocument and self.cimDoc.alternatives:
            w = cimAlternative(self.cimDoc, self._converted, idstring, self.store,2)
            self.widgets['alternatives'] = w
            self.inner.pack_start(w, expand=False, padding=2)

        # Now all those nice buttons at the bottom

        self.bbox = gtk.HBox(spacing=5)
        b = pu.ibutton(gtk.STOCK_SAVE, 'Save %s' % self.cimDoc.cimType)
        self.bbox.pack_start(b, expand=False, padding=5)
        self.saver = b
        b = pu.ibutton(gtk.STOCK_QUIT, 'Quit')
        self.closer = b
        self.bbox.pack_start(b, expand=False, padding=5)
        b=pu.ibutton(gtk.STOCK_DELETE,  'Delete')
        self.killer = b
        self.bbox.pack_start(b, expand=False, padding=5)
        b=pu.ibutton(gtk.STOCK_HELP, 'Help')
        self.helper = b
        self.bbox.pack_start(b, expand=False, padding=5)

        self.vbox.pack_start(self.bbox, expand=False, padding=5)
            
    def __callback(self,key,value):
        ''' Used for actions on the child widgets to alter state '''
        target = self.cimDoc.attributes[key]
        if target.single_valued:
            target.set(value)
        else:
            if value is None:
                target.reset_list([]) # ticket 123
            else:
                target.reset_list(value)
        
    def connect(self, command, callback, origin):
        ''' Connect to a button for saving the entire thing '''
        self.external_callback = callback
        self.saver.connect(command, self._ubercallback, (self.cimDoc, 'Save'))
        self.closer.connect(command, self._ubercallback, (self.cimDoc, 'Quit'))
        self.killer.connect(command, self._ubercallback, (self.cimDoc, 'Delete'))
        self.helper.connect('clicked', self._helper, None)
        self.origin = origin
        
    def _helper(self,widget,data):
        """Show the documentation for this class"""
        cv = ph.classView(self.cimDoc.cimType)
        cv.run()
        
    def _ubercallback(self,widget, data):
        ''' Used to activate all widgets then call external callback'''
        if data[1] == 'Delete':
            # We're removing an entity from being in an attribute list
            print 'Deletion happening', data[0]
        else:
            for w in self.widgets:
                self.widgets[w].activate()
            if self.cimDoc.isDocument:
                instance = data[0]
                instance.update_meta()
        kill_me = self.external_callback(self, data)
        if kill_me:
            self.destroy()

    def _converted(self, widget, new):
        """Called by the alternative widget if the document has been converted"""
        kill_me = self.external_callback(self, (new, widget.chosen))
        self.destroy()
            
    def show(self):
        self.vbox.show()
        self.bbox.show()
        self.inner.show()
        self.sw.show()
        for w in self.widgets:
            self.widgets[w].show()
        self.saver.show()
        self.closer.show()
        self.killer.show()
        self.helper.show()
        super(gtk.Frame, self).show()
        
basicWidgets={'str':{'1.1':strSingle,'0.1':strSingle,'0.N':strMultiBox,'1.N':strMultiBox},
           'int':{'1.1':intSingle,'0.1':intSingle,'0.N':intMulti,'1.N':intMulti},
           'text':{'1.1':txtSingle,'0.1':txtSingle},
           'datetime':{'1.1':strSingle,'0.1':strSingle},
           'bool':{'1.1':nw.BooleanEntry,'0.1':nw.BooleanEntry},
           'float':{'1.1':floatSingle,'0.1':floatSingle},
            }

#
# Section Three: Unit Tests ********************************************
#

def refresh_gui():
    ''' Provides a main loop for unit tests '''
    while gtk.events_pending():
        gtk.main_iteration_do(block=False)

class TestViews(unittest.TestCase):
    
    def setUp(self):
        ''' Create storage for widgets to write to '''
        self.data={}
    
    def testCoverage(self):
        ''' Check we have coverage for all the attribute types declared in
        psuedo_mp classmap '''
        for e in definitions.classmap:
            if e not in basicWidgets:
                raise ValueError('Missing entry type %s'%e)
                            
    def _parent_callback(self,key,value):
        ''' Used for the test callback methods '''
        self.data[key]=value
        print 'Test parent callback set %s with %s'%(key,value)
                
    def test_strSingle(self):
        ''' Test single valued string entry '''
        myproperty='Property'
        someValue='some value'
        # set up widget
        widget=strSingle((myproperty,None,'str','docs'),self._parent_callback)
        # check the label works
        self.assertEqual(widget.label.get_text(),myproperty)
        # check the text entry works
        widget.textEntry.set_text(someValue)
        widget.activate()
        self.assertEqual(self.data[myproperty],someValue)
        
    def test_intSingle(self):
        ''' test entering an integer and only an integer '''
        #show usage
        myproperty='Property'
        self.data[myproperty]=None
        widget=intSingle((myproperty,self.data[myproperty],'int','docs'),self._parent_callback)
        # test outcome
        myvalue=987
        widget.textEntry.set_text(str(myvalue))
        widget.textEntry.activate()
        refresh_gui()
        self.assertEqual(self.data[myproperty],myvalue)
        #
        widget.textEntry.set_text('a word')
        widget.textEntry.activate()
        refresh_gui()
        # shouldn't have changed!
        self.assertEqual(self.data[myproperty],myvalue)
        self.assertEqual(widget.textEntry.get_text(),'%s (Integer!)'%myvalue)
        
    def test_intMulti(self):
        ''' Test multi valued integer '''
        #show usage
        myproperty='Property'
        self.data[myproperty]=[]
        widget=intMulti((myproperty,self.data[myproperty],'int','docs'),self._parent_callback)
        # test outcome
        myint=987
        widget.textEntry.set_text(str(myint))
        widget.textEntry.activate()
        refresh_gui()
        self.assertEqual(self.data[myproperty],[myint])
        #
        myints=[987,123]
        widget.textEntry.set_text(','.join([str(i) for i in myints]))
        widget.textEntry.activate()
        refresh_gui()
        self.assertEqual(self.data[myproperty],myints)
        
    def test_txtSingle(self):
        ''' Test working with a text buffer '''
        # show usage
        myproperty='text'
        mytext1="If I were again beginning my studies, I would follow the advice of Plato and start with mathematics (Galileo Galilei)"
        mytext2="An education isn't how much you have committed to memory, or even how much you know. It's being able to differentiate between what you know and what you don't. (Anatole France) "
        widget=txtSingle((myproperty,mytext1,'text','docs'),self._parent_callback)
        # test outcome
        widget.set_text(mytext2)
        widget.activate()
        self.assertEqual(self.data[myproperty],mytext2)
        
    def testStrBehaviour(self):
        s=strSingle(('test',None,'str','docs'),self._parent_callback)
        #print 's.value [%s]'%s.value
        #print 's.gettext [%s]'%s.textEntry.get_text()


class TestDialog(unittest.TestCase):
    """ Used for a standalone human test interaction with the main dialog"""

    def setUp(self):
        self.tdir = tempfile.mkdtemp()
        self.store = ps.cimStorage(self.tdir)
        author = factory.make_cim_instance('Party')
        author.name = 'Unittest'
        self.store.set_default_author(author)

    def tearDown(self):
        shutil.rmtree(self.tdir)
        gone = os.path.exists(self.tdir)
        self.assertEqual(gone, False)

    def testDialog(self):
        """Test main dialog with a project"""
        def callback(w,d):
            """ Dummy callback for dialog"""
            print w,d
        f = factory.cimFactory()
        test_type = 'Project'
        r = f.build(test_type)
        r.meta = self.store.makeDefaultMeta()
        d = cimwDialog('test_%s' % test_type, r, test_type, callback, 'header', self.store)
        d.run()
        d.destroy()

class TestCimWidgets(unittest.TestCase):
    """ Provides test cases for the CIM doc widgets.
    All we're really interested in is whether or not a
    widget can be properly built or not."""

    def setUp(self):
        self.tdir = tempfile.mkdtemp()
        self.store = ps.cimStorage(self.tdir)
        if test_cmip6:
            self.store.factory.add_package_by_functions(
                'cmip6', cmip6.get_constructors())

    def tearDown(self):
        shutil.rmtree(self.tdir)
        gone = os.path.exists(self.tdir)
        self.assertEqual(gone, False)
        
    def _tester(self, klass):
        klass_instance = self.store.factory.build(klass)
        if self.store.factory.isdoc(klass):
            klass_instance.meta = self.store.factory.build('Meta')
        w = cimwBasic(klass_instance, self.store)
        w.connect('clicked', self.mycallback, 'testing')
        self.button_clicked = 0
        w.saver.clicked()
        self.assertEqual(self.button_clicked, 1,
                         'Failed button %s' % klass_instance.cimType)

    def mycallback(self,widget, data):
        ''' This is the callback we connect to the widget button '''
        self.button_clicked = 1
        payload, origin = data
        self.payload = payload

    def testDocument(self):
        self._tester('Model')

    def testEnum(self):
        self.assertRaises(AssertionError, self._tester, 'ModelTypes')

    def test_cimProperty(self):
        ''' Test working with a cimProperty class '''
        # show usage
        myproperty = 'requirement'
        value = None
        w = cimProperty((myproperty, value, 'numericalRequirement', 'docs'),
                self.mycallback, 'My property title', None)

def create_test(klass):
    ''' Create a test for each cim class ''' 
    #http://stackoverflow.com/questions/2798956/python-unittest-generate-multiple-tests-programmatically       
    # which was immensely clever!
    def do_test_make(self):
        self._tester(klass)
    return do_test_make
   
if __name__ == "__main__":
    factory = factory.cimFactory()
    if test_cmip6:
        factory.add_package_by_functions('cmip6', cmip6.get_constructors())
    for x in factory.classes:
        print x
        test_method = create_test(x)
        test_method.__name__ = 'test creating %s' % x
        setattr(TestCimWidgets, test_method.__name__, test_method)
    unittest.main()
