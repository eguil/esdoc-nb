
__author__ = 'BNL28'
import unittest
import tempfile
import shutil
import os
from collections import OrderedDict

import gtk

import pseudo_utils as pu
import pseudo_storage as ps
import pseudo_linking as pl

import esdoc_nb.mp.core.lib.uml as uml
import esdoc_nb.mp.core.lib.factory as factory
import esdoc_nb.mp.core.lib.definitions as definitions


LINK_BOX_HEIGHT = 35
#
# Utility methods
#
def make_label(umlp, label_length):
    """ Utility method for consistent label making
    :param umlp: a uml property
    :param label_length: length for label
    :return:
    """
    label = gtk.Label(
        '%s\n<span size="x-small">(%s)</span>' % (umlp.name, umlp.target))
    label.set_use_markup(True)
    if label_length != -1:
        label.set_width_chars(label_length)
    tooltip = gtk.Tooltips()
    tooltip.set_tip(label, umlp.doc)
    return label

#
# This is the collection of widgets which are used by the major dialogs in the CIM notebook.
#
class BooleanEntry(gtk.HBox):
    """ A boolean entry box for CIM dialog widgets, which can take the value
    of None, True, or False """

    def __init__(self, umlp, callback, spacing=5, label_length=-1):
        """ Instantiate a boolean entry widget
        :param umlp: a uml property instance
        :paral callback: external callback for value changes
        :param spacing: layout parameter
        :param label_length: if set, control length of label
        :return:
        """
        super(BooleanEntry, self).__init__(spacing=0)

        self.external_callback = callback

        self.umlp = umlp
        self.spacing = spacing
        label = make_label(self.umlp, label_length)
        self.pack_start(label, False, True, self.spacing)

        self.radio()
        self.widget_height = LINK_BOX_HEIGHT

    def radio(self):
        """Makes the radio buttons"""
        assert self.umlp.value in [None, False, True],\
            'invalid option %s for boolean entry radio' % self.umlp.value
        b1 = gtk.RadioButton(None, 'None')
        b1.connect('toggled', self.__changer, None)
        b2 = gtk.RadioButton(b1, 'True')
        b2.connect('toggled', self.__changer, True)
        b3 = gtk.RadioButton(b2, 'False')
        b3.connect('toggled', self.__changer, False)
        self.buttons = OrderedDict([(None, b1), (True, b2), (False, b3)])
        self.buttons[self.umlp.value].set_active(True)
        for b in self.buttons:
            self.pack_start(self.buttons[b], padding=self.spacing, expand=False)

    def __changer(self, widget, data = None):
        """ Respond to toggling of radio buttons """
        # We only care about the toggle ON, since toggle off precedes it.
        if widget.get_active():
            assert self.external_callback is not None
            print 'updating toggled %s=%s' % (self.umlp.name,data)
            self.external_callback(self.umlp.name, data)

    def show(self):
        self.show_all()

class EntryButtonBox(gtk.HBox):
    """ Provides a button box which can be used for single entries or
    in common for multiple entries"""

    def __init__(self, spacing):
        super(EntryButtonBox, self).__init__(spacing=0)
        self.my_buttons = OrderedDict()
        self.spacing = spacing

        self.button_set = {
            'add': (gtk.STOCK_ADD, '', 'Add entries'),
            'edit': (gtk.STOCK_EDIT, '', 'Edit entries'),
            'clean': (gtk.STOCK_CLEAR, '', 'Clear entries')
        }

    def set_buttons(self, buttons):
        """ Set buttons visible and usable in button box
        :param buttons: Ordered List of Required Buttons
            from this list: ['Add','Edit','Clean']
        """
        if self.my_buttons:
            # Clear existing buttons
            for b in self.my_buttons:
                self.my_buttons[b].destroy()
            self.my_buttons = OrderedDict()
        if not buttons: return
        for b in buttons:
            assert b in self.button_set, 'What button? %s' % b
            button = pu.ibutton(*self.button_set[b])
            self.my_buttons[b] = button
            self.pack_start(button, False, False, self.spacing)

    def connect(self, adder=None, cleaner=None, editer=None):
        """Connect callbacks to the buttons as required externally"""
        if adder:
            assert 'add' in self.my_buttons
            self.my_buttons['add'].connect('clicked', adder, self)
        if cleaner:
            assert 'clean' in self.my_buttons
            self.my_buttons['clean'].connect('clicked', cleaner, self)
        if editer:
            assert 'edit' in self.my_buttons
            self.my_buttons['edit'].connect('clicked', editer, self)


class IndividualEntry(gtk.HBox):
    """Provides a box into which a text entry and some buttons are placed. These
    boxes are then added into an EntryBox VBox """

    def __init__(self, max_entry_length=0, spacing=5):
        """ Constructor for individual entry box
        :param entry_length: gtk.Label maximum entry length
        :param spacing: Default padding and spacing
        :return:
        """
        super(IndividualEntry, self).__init__(max_entry_length, spacing=spacing)

        self.textEntry = gtk.Entry()
        self.textEntry.set_max_length(max_entry_length)

        self.pack_start(self.textEntry, True, True, 0)

        self.spacing = spacing

        self.buttons = EntryButtonBox(spacing)
        self.pack_start(self.buttons, False, False, 0)

    def set_value(self, value):
        """ Set a value in the entry box """
        self.value = value
        if value:
            assert isinstance(value, str)
            self.textEntry.set_text(value)

    def editable(self, value):
        """ Set entry editable state
        (usually used for setting not editable for CIM values)"""
        self.textEntry.set_editable(value)
        self.textEntry.modify_base(gtk.STATE_NORMAL,
                                   gtk.gdk.color_parse('#E7E7E9'))


class LinkEntryBox(gtk.HBox):
    """ Provides a box which exposes the entry API for a property in the notebook """

    def __init__(self, umlp, store, callback, idstring='entry', entry_length=0, label_length=-1):
        """ Constructor for the basic entry box
         :param umlp: A uml property which needs to be set or changed
         :param store: the CIM database store (to find docs for linking)
         :param callback: Called by internal callback to set property on parent document
         :param idstring: A heading for the dialog box
         :param entry_length: The maximum length of the entry itself
         :param label_length: Maximum length of the label itself
         :return: widget
        """
        self.spacing = 5
        self.right_space = 2
        super(LinkEntryBox, self).__init__(spacing=self.spacing)

        self.max_entry_length = entry_length
        self.umlp = umlp
        self.idstring = idstring
        self.store = store
        self.widgets = []
        self.external_callback = callback
        self.__set_label_and_entry(label_length)
        self.widget_height = LINK_BOX_HEIGHT

        assert umlp.islink, "EntryBox currently only supports cim links"

    def __set_label_and_entry(self, label_length):
        """ Set the label and type """
        self.label = make_label(self.umlp, label_length)
        self.pack_start(self.label, False, True, self.spacing)

        self.pack_start(gtk.VSeparator(), True, False, 0)

        # The vbox is where we will be putting all the entries
        self.vbox = gtk.VBox(spacing=self.spacing)
        self.pack_start(self.vbox, True, True, 0)

        # self.pack_start(gtk.VSeparator(), True, False, self.spacing)
        # Now a buttonbox on the right of the vbox
        self.buttons = EntryButtonBox(self.spacing)
        self.pack_start(self.buttons, False, False, 0)

        self.set_value(self.umlp.value)

    def set_value(self, newvalue):
        """ Set a value in the entry box
        :param umlp: content to be placed in the box (needs to be uml property)
        """

        for w in self.widgets: w.destroy()

        if self.umlp.single_valued:
            self.__set_single()
        else:
            self.__set_multi()

        for w in self.widgets: w.show_all()
        self.buttons.show_all()

    def __set_single(self):
        """Set a single value to a single row """
        row = IndividualEntry(self.max_entry_length, self.spacing)
        row.set_value(str(self.umlp.value))
        self.widgets.append(row)
        self.vbox.pack_start(row, False, True, self.spacing)
        if self.umlp.target not in definitions.classmap:
            # we need buttons
            row.editable(False)
            if self.umlp.value:
                row.buttons.set_buttons(['edit', 'clean'])
                row.buttons.connect(editer=self.editer)
                row.buttons.connect(cleaner=self.cleaner)
            else:
                row.buttons.set_buttons(['edit', ])
                row.buttons.connect(editer=self.editer)

    def __set_multi(self):
        """ Set a multiple valued property to multiple rows """

        # There are four interesting cases:
        #   (1) A set of simple instances, we can edit those in place
        #       add a clear button, and a plus sign on the last.
        #   (2) A set of complex cim instances, which are not document
        #       classes. We expect to edit those by popping up a
        #       a dialog.
        #   (3) A set of document instances, we don't edit those, but
        #       simply pop up a chooser for manipulating them.
        #   (4) A set of enums.
        #
        # However, this version of the code only deals with (3)

        # Start with an empty unit, this is the easiest case.
        if not self.umlp.value:
            row = IndividualEntry(self.max_entry_length, self.spacing)
            row.editable(False)
            self.vbox.pack_start(row, False, True, self.spacing)
            self.buttons.set_buttons(['add', ])
            self.buttons.connect(adder=self.editer)
            self.widgets.append(row)
        else:
            # ok, we've got values to deal with
            for v in self.umlp.value:
                row = IndividualEntry(self.max_entry_length, self.spacing)
                row.set_value(str(v))
                self.vbox.pack_start(row, False, True, self.spacing)
                row.editable(False)
                self.widgets.append(row)

            # Now pack our button box, not the row button box
            self.buttons.set_buttons(['edit', 'clean', ])
            self.buttons.connect(editer=self.editer)

    def editer(self, w, b):
        """This is the method which is called by the 'add' and 'edit' buttons
        to pop up the cimlink document chooser dialog : cimLinkDialog"""
        print 'Checkconstraint', self.umlp.constrained
        self.linkDialog = pl.cimlinkDialog(self.umlp, self.setme, self.idstring, self.store)
        self.linkDialog.run()

    def cleaner(self, w, b):
        """This is the method used to clear value """
        # We're not allowed to clean metadata records away, since we lose the UID!
        # This should be avoided elsewhere, this is our last chance to stop it.
        assert self.umlp.cimType != 'meta'
        self.setme(w, None)

    def setme(self, w, v):
        """This is the routine provided as the callback to the popup dialog,
        it sets the value of the uml property using the content of the callback."""

        self.umlp.set(v)
        self.set_value(v)

        self.linkDialog.destroy()
        self.external_callback(self.umlp.name, v)

    def get_value(self):
        """ Get the value of the uml property displayed in the widget"""
        return self.umlp.value

    def show(self):
        self.show_all()


class TestIndividualEntry(unittest.TestCase):
    def setUp(self):
        self.d = gtk.Dialog('unit testing')
        self.ie = IndividualEntry()
        self.d.vbox.pack_start(self.ie)

    def testSetIndEntry1(self):
        self.ie.set_value('Should show edit and clear buttons')
        self.ie.buttons.set_buttons(['edit', ])
        self.d.show_all()
        self.d.run()

    def testSetIndEntry12(self):
        self.ie.set_value('Should show add button only')
        self.ie.buttons.set_buttons(['edit', ])
        self.ie.buttons.set_buttons(['add', ])
        self.d.show_all()
        self.d.run()

class TestBooleanEntry(unittest.TestCase):
    def setUp(self):
        self.d = gtk.Dialog('unit testing')
        egp = 'is_required', 'bool', '1.1', 'property is required'
        self.umlp = uml.umlProperty(egp)

    def testBooleanEntry(self):
        def callback(p,v):
            print 'Radio for %s toggled to %s' % (p,v)
            self.umlp.set(v)
        self.be = BooleanEntry(self.umlp,callback)
        self.d.vbox.pack_start(self.be)
        self.d.show_all()
        self.d.run()


class TestEntryBox(unittest.TestCase):
    def setUp(self):
        self.d = gtk.Dialog('unit testing')
        egp = 'sub_project', 'linked_to(activity.Project)', '1.N', 'Sub project docs'
        self.umlp = uml.umlProperty(egp)
        self.umlq = uml.umlProperty(egp)
        self.tdir = tempfile.mkdtemp()
        self.store = ps.cimStorage(self.tdir)
        author = factory.make_cim_instance('Party')
        self.store.set_default_author(author)
        for i in range(5):
            doc = factory.make_cim_instance('Project')
            doc.name = 'Test Project %s' % i
            meta = factory.make_cim_instance('Meta')
            doc.meta = meta
            self.store.add(doc)
            link = self.store.link_to_doc(doc)
            self.umlq.append(link)

    def tearDown(self):
        shutil.rmtree(self.tdir)
        gone = os.path.exists(self.tdir)
        self.assertEqual(gone, False)

    def testEmptyEntryBoxDocList(self):
        def callback(w,d):
            print w,d
        eb = LinkEntryBox(self.umlp, self.store, callback)
        eb.set_value(self.umlp.value)
        self.d.vbox.pack_start(eb)
        self.d.show_all()
        self.d.run()

    def testEntryBoxDocList(self):
        def callback(w,d):
            print w,d
        eb = LinkEntryBox(self.umlq, self.store, callback)
        eb.set_value(self.umlq.value)
        self.d.vbox.pack_start(eb)
        self.d.show_all()
        self.d.run()
