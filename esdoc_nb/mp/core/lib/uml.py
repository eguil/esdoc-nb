import collections
import uuid, datetime

from meta import checkmetamodel, PropertyType
from definitions import classmap
from utils import make_string
import copy

class umlProperty(object):
    """ Implements a property which follows our pythonic uml property assertions.
    Property can be constrained to an unchangeable value by construction"""
    def __init__(self, attribute, value=None):
        """
        :param attribute: a metamodel property statement (e.g. ('name','type','1.1','docs')
        :param value: an initial value of the attribute (making this property fixed).
        :return: umlProperty
        """
        self.uml_attribute = attribute
        # make sure it's a valid property these assertions define our property metamodel
        try:
            assert isinstance(self.name, str) or isinstance(self.name, unicode), 'Invalid Name Failure %s ' % self.name
            assert self.cardinality in ['0.1', '1.1', '1.N', '0.N'], "Cardinality Failure - [%s]" % self.cardinality
        except AssertionError, e:
            raise ValueError('%s is not a valid property definition - [%s]' %
                                (attribute,  str(e)))
        if self.single_valued:
            self.__value = None
        else:
            self.__value = []

        if value:
            self.__value = value
            self.constrained = True
        else:
            self.constrained = False

    @property
    def isFixed(self):
        """Constraint property may make this uml property unchangeable """
        if self.constrained: return 1
        return 0

    @property
    def value(self):
        return self.__value

    @property
    def name(self):
        return self.uml_attribute[0]

    @property
    def target(self):
        return str(self.uml_attribute[1])

    @property
    def fulltarget(self):
        """ The target including package name"""
        return self.uml_attribute[1].target

    @property
    def cardinality(self):
        return self.uml_attribute[2]

    @property
    def doc(self):
        return self.uml_attribute[3]

    @property
    def single_valued(self):
        return self.cardinality in ['0.1', '1.1']

    @property
    def valid(self):
        if self.cardinality.startswith('1'):
            if self.value in [None, []]:
                return 0
        return 1

    @property
    def islink(self):
        return self.uml_attribute[1].link

    def __myType(self, other):
        """Check other object for consistency with me """
        if self.target in classmap:
            if other.__class__ not in classmap[self.target]:
                if self.target == 'int' and other.__class__ == float:
                    if float(int(other)) == other: return 1
                return 0
            return 1
        if not (isinstance(other, umlClass) or isinstance(other, umlEnum)): return 0
        if self.islink and isCIMinstance(other, 'OnlineResource'): return 1
        if isCIMinstance(other, self.target): return 1
        return 0

    def set(self,v):
        """ Set value """

        def wrongtype(v):
            """ Convenience method for error message"""
            if isinstance(v, umlClass) or isinstance(v, umlEnum):
                msgname = v.cimType
            else:
                msgname = v
            raise ValueError(
                    'Attempt to set <%s> with wrong type <%s>' % (self.name, msgname))

        if self.isFixed:
            return AttributeError("Attempt to change constrained fixed value %s"%self)

        if self.single_valued:
            if v is None:
                self.__value = None
                return
            if self.__myType(v):
                self.__value = v
            else:
                wrongtype(v)
        else:
            if isinstance(v,list):
                if v == []:
                    self.__value = v
                else:
                    for vv in v:
                        if not self.__myType(vv):
                            raise ValueError('Invalid data %s(%s) for %s'%(vv,v,self.target))
                    self.__value = v
            else:
                raise ValueError('Attempt to set a value to a list property')

    def append(self,v):
        """ Append value if appropriate """
        if self.single_valued:
            raise ValueError('Cannot append to %s'%self.name)
        self.__value.append(v)

    def reset_list(self,v):
        if self.single_valued:
            raise ValueError('Cannot reset list for %s'% self.name)
        self.__value = v

    def __eq__(self, other):
        if self.uml_attribute != other.uml_attribute: return 0
        if self.value != other.value: return 0
        return 1

    def __ne__(self, other):
        return not self == other

    def __str__(self):
        return str(self.uml_attribute)


class umlClass(object):
    """ Implements a class which follows our pythonic uml class assertions.
    Note that if it has constraints, these will be fixed in the factory
    after instantiation of the raw class."""

    def __init__(self, constructor, cim_constructor_set):
        """
        :param constructor: A dictionary of class characteristics from the known classes in
        :param the cim_constructor_set.
        :param initialise_document: Boolean: If a document, optionally initialise internal metadata
        """
        # uml attributes
        super(umlClass, self).__setattr__('attributes', collections.OrderedDict())

        self.known = cim_constructor_set

        # class hierarchy:
        self.base = []
        # alternative classes (including sub-classes):
        self.alternatives = []
        # documentation for this class:
        self.doc = ''
        # serialisations, including print methods
        self.serialisations = []

        self.cimType = constructor['cimType']

        self._parseConstructor(constructor)

        if self.cimType in self.alternatives:
            index = self.alternatives.index(self.cimType)
            base = str(constructor['base'])
            self.alternatives[index] = base

        # other known attributes which we want to keep
        for a in ['pstr','version']:
            if a in constructor:
                setattr(self, a, constructor[a])

        # assert metamodel
        checkmetamodel(constructor)

    @property
    def isDocument(self):
        """Implementing the document stereotype"""
        if 'meta' in self.attributes:
            return 1
        else:
            return 0

    @property
    def uid(self):
        """ Access internal metadata uid """
        assert self.isDocument,'Attempt to find uid of %s (not a document)' % self.cimType
        return self.meta._internal_metadata.id

    def __setattr__(self, k, v):
        """Override setattribute to ensure we have valid attributes
        where an attribute is a cim attribute"""
        if k in self.attributes:
            self.attributes[k].set(v)
        else:
            super(umlClass, self).__setattr__(k, v)

    def __getattribute__(self, item):
        """ Override getattribute to get items from the attribute
        store if appropriate """
        try:
            attributes = super(umlClass, self).__getattribute__('attributes')
        except AttributeError:
            # This shouldn't arise in normal execution, but when an instance is copied
            # using deepcopy, it somehow can ... although I don't know how the
            # instance can be constructed without using the constructor.
            attributes = []
        if item in attributes:
            return attributes[item].value
        else:
            return super(umlClass, self).__getattribute__(item)

    def _parseConstructor(self,constructor):
        """Parse constructor and load attributes"""

        # start with class hierarchy
        base_class = constructor['base']
        if base_class is not None:
            # Mark defines base classes with package included,
            # I don't ... so handle both for now.
            bc = str(base_class)
            self._parseConstructor(self.known[bc])
            self.base.append(bc)

        # collect constraints
        self.constraint_set = {}
        if 'constraints' in constructor:
            for constraint in constructor['constraints']:
                self.constraint_set[constraint[0]] = constraint[1:]

        # now deal with the uml attributes
        # (some constructors may be constraint only)
        if 'properties' in constructor:
            for a in constructor['properties']:
                p = umlProperty(a)
                self.attributes[a[0]] = p

        # doc string is a special case:
        assert 'doc' in constructor
        self.doc += ':%s' % constructor['doc']

        # aggregate alternatives
        if 'alternatives' in constructor:
            self.alternatives +=  constructor['alternatives']

    def update_meta(self):
        """ Update last updated and version for a document """
        assert self.isDocument,'Attempt to update metadata for %s (not a document)' % self.cimType
        imeta = self.meta._internal_metadata
        imeta.version += 1
        imeta.update_date = datetime.datetime.now().isoformat(' ')

    def copy(self):
        """ Return a copy with new UID and document version"""
        new = copy.copy(self)
        new.name = 'Copy of %s' % str(new)
        imeta = new.meta._internal_metadata
        imeta.version = 1
        imeta.creation_date = datetime.datetime.now().isoformat(' ')
        imeta.id = str(uuid.uuid1())
        return new

    def validate(self, quiet=False):
        """ Are all the required CIM attributes present?"""
        missing=[]
        for p in self.attributes:
            if not self.attributes[p].valid: missing.append(p)
        if missing == []:
            return 1
        else:
            if quiet:
                return 0
            else:
                raise ValueError('%s missing attributes %s' % (self.cimType, missing))


    @property
    def links(self):
        """Method to return all the outbound cim links for this instance as triples"""
        links = []
        for p in self.attributes:
            umlp = self.attributes[p]
            if umlp.islink and umlp.value:
                if umlp.single_valued:
                    value = (self, umlp.name, umlp.value)
                    links.append(value)
                else:
                    for v in umlp.value:
                        value = (self, umlp.name, v)
                        links.append(value)
        return links

    @property
    def pyesdoc(self):
        """ Method to return a pyesdoc version for printing, storing etc """
        # Start by instantiating a pyesdoc class and returning that, this is a work in progress
        try:
            klass = getattr(esdoc, self.cimType)
        except:
            raise ValueError('Cannot instantiate esdoc class %s ' % self.cimType)
        esdoc_class = klass()
        for a in self.attributes:
            umlp = self.attributes[a]
            value = umlp.value
            if value is not None:
                if umlp.target not in classmap:
                    if umlp.single_valued:
                        value = value.pyesdoc
                    else:
                        value = [v.pyesdoc for v in value]
            if 0: print 'Setting %s with %s' % (umlp.target, value)
            setattr(esdoc_class, umlp.name, value)
        return esdoc_class


    def __str__(self):
        """ String version """
        return make_string(self)

    def __eq__(self,other):
        """ Class and member equality """
        if not isinstance(other, self.__class__): return 0
        if self.cimType != other.cimType: return 0
        if len(self.__dict__) != len(other.__dict__): return 0
        for k in self.__dict__:
            if getattr(self, k) != getattr(other, k): return 0
        return 1

    def __ne__(self,other):
        """ Class and member inequality """
        return not self == other


class umlEnum(object):
    """ A CIM vocabulary """

    def __init__(self, constructor):
        """ Construct and return a vocabulary ENUM from an mp entity description"""
        self.members = {}
        for m in constructor['members']:
            self.members[m[0]] = m[1]
        if constructor['base'] is not None:
            raise ValueError('Code for subclassing enums not yet available')
        for a in constructor:
            if a not in ['base', 'members']:
                setattr(self, a, constructor[a])
        self.value = self.members.keys()[0]
        self.cimType = constructor['cimType']

    def set(self, value):
        if value in self.members:
            self.value = value
        else:
            raise ValueError(
                'Attempt to set value <%s> of enum <%s> outside membership' %
                (value, self.cimType))

    def __str__(self):
        return self.value

    def __eq__(self, other):
        if not isinstance(other, umlEnum): return 0
        if self.members == other.members and self.value == other.value: return 1
        return 0

    def __ne__(self, other):
        return not self == other

    @property
    def pyesdoc(self):
        """ Method to return a pyesdoc version for printing, storing etc """
        #pysesdoc only cares about my value ...
        return self.value


def isCIMinstance(instance, rtype):
    """ CIM aware version of python isinstance, ensures that cim instances
    respect the notional class inheritance. """
    if isinstance(instance, umlClass):
        class_tree = [instance.cimType, ] + instance.base
        if rtype in class_tree:
            return 1
        else:
            return 0
    elif isinstance(instance, umlEnum):
        assert not hasattr(instance,'base'),"isCIMinstance doesn't support enums with base classes"
        return instance.cimType == rtype
    else:
        ok = max([isinstance(instance, r) for r in classmap[rtype]])
        return ok