from inspect import getmembers, isfunction
import unittest
from copy import copy

from definitions import classmap, extension_packages

def camelize(string):
    x = string.split('_')
    if len(x) > 1:
        y = [s.capitalize() for s in x]
    else:
        y = [x[0][0:1].capitalize(),x[0][1:]]
    return ''.join(y)


class PropertyType:
    """ Defines the properties of a "target" type string as it appears in the schema definitions in a property
    definition (e.g. ('property_name','type_string','cardinality','definition').
    """
    def __init__(self, type_string):
        """
        :param type_string: A schema definition type string
        :return: An object with the following attributes:
           link: boolean: target will be represented by a "doc_reference" element rather than the actual element.
           package: string: the package in which the target type exists
           property_type: the actual target property type.
           camel: target property type converted to camel case
           link_constraint_type: a constraint type on a remote link, if provided.
        """
        self.link = False
        self.link_constraint_type = None
        self.pythonic = False
        self.definition = type_string
        assert type_string[0] != ' ', 'Property definitions must not begin with a blank: [%s] ' % type_string

        if type_string in classmap:
            self.pythonic = True
            self.package = None
            self.camel = type_string
            self.property_type = type_string
            self.target = type_string
        else:
            if type_string.startswith('linked_to'):
                self.link = True
                content = type_string[10:-1]
                if content == '':
                    # any possible document type
                    self.package = 'Any'
                    self.camel = 'Any'
                    self.target = 'Any'
                    return
                else:
                    content = content.split(',')
                if len(content) > 1:
                    self.link_constraint_type = PropertyType(content[1])
                content = content[0]
            else:
                content = type_string
            twoparts = content.split('.')
            assert len(twoparts) == 2, 'Invalid property definition - expected package.property_type, got [%s] ' % content
            self.package, self.property_type = twoparts
            self.camel = self._camelise(self.property_type)
            self.target = '%s.%s' % (self.package, self.camel)

    def _camelise(self, dname):
        """ Capitalise ourself """
        if dname in classmap:
            return dname
        return camelize(dname)

    def __eq__(self, other):
        properties = ['link', 'property_type', 'camel', 'package', 'link_constraint_type']
        for p in properties:
            if getattr(self, p) != getattr(other, p): return False
        return True

    def __ne__(self, other):
        return not self == other

    def __str__(self):
        return self.camel


def checkmetamodel(constructor, cimset=None):
    """
    Checks consistency of a class constructor with our metamodel
    :param constructor: A cim class definition constructor
    :param cimset: The complete set of possible cim targets for property target testing
    :return: True if OK
    """
    try:
        # Next two attributes are not in the basic upstream definition functions but are inserted
        # by the esdoc-nb/mp framework to support the downstream API.
        assert 'cimType' in constructor, 'Missing cimType'
        assert 'doc' in constructor, 'Missing doc'
        # The rest of these tests are the basic upstream metamodel understood by esdoc-mp
        assert 'is_abstract' in constructor,'Missing is_abstract'
        assert 'properties' in constructor or 'constraints' in constructor, 'Missing properties'
        assert 'base' in constructor, 'Missing base'
        if 'properties' in constructor:
            assert isinstance(constructor['properties'], list), 'Properties not a list'
            for p in constructor['properties']:
                if cimset:
                    assert str(p[1]) in cimset or str(p[1]) in classmap.keys(), \
                        'property target failure for %s' % p[1]
        if 'constraints' in constructor:
            assert isinstance(constructor['constraints'], list), "Constraints not a list"
            for c in constructor['constraints']:
                 assert c[1] in ['value', 'from', 'hidden', 'include', 'cardinality'],\
                    'Constraint failure %s' % c[1]
                 if cimset:
                    targets = {}
                    available = gather_base_properties(constructor, cimset)
                    for a in available:
                        targets[a[0]] = a[1]
                    assert c[0] in targets,\
                        'Constraint on non-existent property %s (%s)' % (c[0], targets)
                    if c[1] == 'include':
                        # check the include classes are of the right type for the property target
                        assert isinstance(c[2],list),'Include constraint not a list %s' % c
                        for t in c[2]:
                            # now we're looping over the includes
                            # check the CIM even knows about it
                            assert str(t) in cimset,'Constraint include target not known %s %s' % (
                                str(t), sorted(cimset.keys()))
                            # now check it's type
                            assert cimset[str(t)] in gather_base_heirarchy(str(t), cimset)

        if 'alternatives' in constructor:
            # Can't rely on this if constructor supplied via factory ...
            assert isinstance(constructor['alternatives'],list), 'Alternatives not a list'
            if cimset:
                for p in constructor['alternatives']:
                    assert str(p) in cimset, 'Non existent alternative class [%s]' % p

    except AssertionError,e:
        print constructor
        raise ValueError('%s fails metamodel test (%s)' % (constructor['cimType'], str(e)))
    except Exception, e:
        print constructor
        import traceback
        traceback.print_exc()
        raise Exception(e)
    return 1

def gather_base_properties(constructor, cimset):
    """ Gather all properties (included inherited properties) for a class
    :param klass: A cim class constructor
    :param cimset: A complete set of cim class constructors
    :return: List of all properties for the given klass
    """
    assert 'base' in constructor,'No base class in constructor %s' % constructor
    r = []
    if 'properties' in constructor:
        r = copy(constructor['properties'])
    bases = gather_base_heirarchy(constructor, cimset)
    for b in bases:
        if 'properties' in cimset[b]:
            r += copy(cimset[b]['properties'])
    return r

def gather_base_heirarchy(constructor, cimset, follow=True):
    """For a given constructor, find the base hierarchy from within a complete set of
     constructors, cimset."""
    if 'members' in constructor:
        return []   # it's an enum
    assert 'base' in constructor, 'No base class in constructor %s' % constructor
    stack = []
    if constructor['base']:
        base = str(constructor['base'])
        stack.append(base)
        if follow:
            assert base in cimset, 'Unknown base class [%s] found in [%s]' % (
                base, constructor['cimType'])
            stack += gather_base_heirarchy(cimset[base], cimset, follow)
    return stack

def function2dict(f):
    """ Used to go from the cim defined as functions to the cim defined as a dictionary
    :param f: a constructor function
    :return: (key,c) a cimtype and it's constructor dictionary
    """
    key = camelize(f[0])
    try:
        c = f[1]()
        if f[1].__doc__ != '':
            c['doc'] = f[1].__doc__
        else:
            # If no real doc string,
            # assume the class name is useful in it's own right
            c['doc'] = key.replace('_',' ')
        c['cimType'] = key
        # The metamodel expects these, but they get in the way of
        # vocabulary construction (since they have no value in
        # normal vocabularies.
        if 'vocab_status' in c:
            if 'properties' not in c:
                c['properties'] = []
            if 'is_abstract' not in c:
                c['is_abstract'] = False
    except Exception, e:
        raise Exception, str(e)+'( for %s)' % key
    return key, c


def fix_properties(constructor):
    """Take a raw constructor and replace all the property names with PropertyTypes """
    constructor = copy(constructor)
    try:
        if 'base' in constructor:
            if constructor['base']:
                constructor['base'] = PropertyType(constructor['base'])
        if 'alternatives' in constructor:
            constructor['alternatives'] = [str(PropertyType(a)) for a in constructor['alternatives']]
        if 'properties' in constructor:
            fixed = []
            for p in constructor['properties']:
                buf = list(p)
                buf[1] = PropertyType(buf[1])
                fixed.append(tuple(buf))
            constructor['properties'] = fixed
        if 'constraints' in constructor:
            for c in constructor['constraints']:
                if c[1] == 'include':
                    c[2] = [str(PropertyType[c2item]) for c2item in c[2]]
    except:
        print 'Failed to fix_properties for constructor'
        print constructor
        raise
    return constructor


def get_from_modules(pdict):
    """ Load constructors from a dictionary of modules """
    c = {}
    pkg = {}
    for p in pdict:
        module = pdict[p]
        function_list = [o for o in getmembers(module) if isfunction(o[1])]
        pkg[p] = []
        for f in function_list:
            key, constructor = function2dict(f)
            c[key] = fix_properties(constructor)
            pkg[p].append(key)
    return c, pkg

def decode_extensions(package_name):
    """ Decode an extension package in the compact notation and turn into standard form
    constructors"""
    assert package_name in extension_packages,'Unknown extension package %s ' % package_name
    constructors = {}
    names = []
    for klass in extension_packages[package_name]:
        compact = copy(klass[1])
        name = camelize(klass[0])
        # unpack compact definitions
        if 'members' in compact:
            compact['type'] = 'enum'
            compact['base'] = None
            compact['doc'] = compact['name']
            compact['cimType'] = name
        else:
            compact['type'] = 'class'
            compact['cimType'] = name
            compact['is_abstract'] = False
            compact['base'] = compact['base']
            compact['doc'] = '(cmip6 constrained version of %s)' % compact['base']
            # how handle implied constraints
            if 'values' in compact:
                compact['constraints'] = []
                for key in compact['values']:
                    compact['constraints'].append((key, 'value', compact['values'][key]))
        constructors[name] = fix_properties(compact)
        names.append(name)
    return names, constructors


class TestPackageSplit(unittest.TestCase):
    """ test all the various possibilites for a type_string ... """

    bad_strings = ['numerical_experiment',
                   'linked_to(scientific_domain)',
                   'linked_to(designing.numerical_experiment,constraint_vocab)']

    # good strings:
    normal = 'designing.numerical_experiment'
    linked = 'linked_to(science.scientific_domain)'
    complicated = 'linked_to(designing.numerical_experiment,designing.constraint_vocab)'

    def test_bad_strings(self):
        for string in self.bad_strings:
            self.assertRaises(AssertionError, PropertyType, string)

    def test_normal(self):
        x = PropertyType(self.normal)
        self.assertEqual(x.property_type, 'numerical_experiment')
        self.assertEqual(x.camel, 'NumericalExperiment')
        self.assertEqual(x.package, 'designing')
        self.assertFalse(x.link)

    def test_linked(self):
        x = PropertyType(self.linked)
        self.assertEqual(x.property_type,'scientific_domain')
        self.assertTrue(x.link)

    def test_complicated(self):
        x = PropertyType(self.complicated)
        self.assertTrue(x.link)
        y = PropertyType('designing.constraint_vocab')
        self.assertEqual(x.link_constraint_type, y)


if __name__=="__main__":
    unittest.main()