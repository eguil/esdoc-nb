__author__ = 'BNL28'


def _cmip6_realms():
    """ Set of canonical scientific domains used in CMIP6 """
    #FIXME:Incomplete
    return {
        'type': 'enum',
        'is_open': False,
        'vocab_status': 'initial',
        'members': [
            ('Atmosphere','Global Atmosphere'),
            ('Ocean','Global Ocean'),
            ('Sea Ice','Sea Ice processes in and/or on the Atmosphere/Ocean'),
            ('Land','Land surface processes'),
     ]
    }

def _cmip6_model_names():
    """ Set of model names registered with CMIP6 """
    return {
        'type': 'enum',
        'is_open': False,
        'vocab_status': 'initial',
        'members': [
        ]
    }

def _cmip6_institute_names():
    """ Set of institute names registered with CMIP6 """
    # FIXME: Consider a registry of parties?
    return {
        'type': 'enum',
        'is_open': False,
        'vocab_status': 'initial',
        'members': [
        ]
    }

