__author__ = 'eguil'
version = '0.0.4'

#
# CMIP6 ocean advection CV
#
# Version history on git/bitbucket#
#
# Top level process
#

ID = 'cmip6.ocean.advection'

PROPERTIES = {
    'type': 'science.process',


    # ----------------------------------------------------------------
    # OVERVIEW
    # ----------------------------------------------------------------
    'short_name': 'Ocean advection',
    'description': 'Properties of ocean advection processes within the ocean component',

    'sub_processes': ['momemtum_adv',
                      'tracer_lateral_adv',
                      'tracer_vertical_adv'],

    # ----------------------------------------------------------------
    # TOP-LEVEL DETAILS 
    # ----------------------------------------------------------------
  
    # ----------------------------------------------------------------
    # SUB-PROCESSES
    # ----------------------------------------------------------------

    'momemtum_adv': {
        'short_name': 'Ocean momemtum advection',
        'description': 'Key properties of momemtum advection in the ocean',
        'details': ['mom_adv_scheme'],
    },


    'tracer_lateral_adv': {
        'short_name': 'Ocean tracer lateral advection',
        'description': 'Key properties of lateral tracer advection in the ocean',
        'details': ['lat_tra_adv_scheme'],
    },

    'tracer_vertical_adv':  {
        'short_name': 'Ocean tracer vertical advection',
        'description': 'Key properties of vertical tracer advection in the ocean',
        'details': ['vert_tra_adv_scheme'],
    },

    # ----------------------------------------------------------------
    # DETAILS OF SUB PROCESSES
    # ----------------------------------------------------------------
    'mom_adv_scheme':  {
        'description': 'Properties of lateral momemtum advection scheme in ocean',
        'short_name': 'Properties of lateral momemtum advection scheme in ocean',
         'scheme': ('ENUM:adv_mom_scheme_types', '1.1'),
        'mom_adv_scheme_name': (
            'str', '1.1',
            'Name of ocean momemtum advection scheme'),
        'mom_adv_ALE': (
            'bool', '1.1',
            'Using ALE for vertical advection ? (if vertical coordinates are sigma)'),         
    },

    'mom_adv_scheme': {
        'description': 'Properties of lateral momemtum advection scheme in ocean',
        'short_name': 'Properties of lateral momemtum advection scheme in ocean',
        'scheme': ('ENUM:adv_mom_scheme_types', '1.1'),
        'mom_adv_scheme_name': (
            'str', '1.1',
            'Name of ocean momemtum advection scheme'),
        'mom_adv_ALE': (
            'bool', '1.1',
            'Using ALE for vertical advection ? (if vertical coordinates are sigma)'),
    },

    'lat_tra_adv_scheme' = {
        'description':'Properties of lateral tracer advection scheme in ocean',
        'short_name': 'Properties of lateral tracer advection scheme in ocean', 
        'scheme': ('ENUM:adv_tra_scheme_types', '1.1'),
        'lat_tra_adv_flux_limiter': (
            'bool', '1.1',
            'Monotonic flux limiter for lateral tracer advection scheme in ocean ?'),
    },

    'vert_tra_adv_scheme': {
        'description': 'Properties of vertical tracer advection scheme in ocean',
        'short_name': 'Properties of vertical tracer advection scheme in ocean', 
        'scheme': ('ENUM:adv_tra_scheme_types', '1.1'),
        'flux_limiter': (
            'bool', '1.1',
            'Monotonic flux limiter for vertical tracer advection scheme in ocean ?'),
    },
}

ENUMS = {
    'mom_adv_scheme_type': {
        'name': 'Type of lateral momemtum advection scheme in ocean',
        'members': [
            ('Flux form', 'tbd'),
            ('Vector form', 'tbd')
        ]
    },
    
    'tra_adv_scheme_type': {        
        'name': 'Type of tracer advection scheme in ocean',
        'members':[
            ('Centred 2nd order', 'tbd'),
            ('Centred 4th order', 'tbd'),
            ('Total Variance Dissipation (TVD)', 'tbd'),
            ('MUSCL', 'tbd'),
            ('QUICKEST', 'tbd'),
            ('Piecewise Parabolic method', 'tbd'),
            ('Sweby', 'tbd'),
            ('Prather 2nd moment (PSOM)', 'tbd'),
            ('Other', 'tbd')
        ]
    },    
}
