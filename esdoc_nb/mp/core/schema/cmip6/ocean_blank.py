__author__ = 'eguil'
version = '0.0.1'

#
# CMIP6 ocean XXXXX CV
#
# Version history on git/bitbucket#
#
# Top level process
#
ocean_ = {
    'base': 'science.process',
    'values': {
        'name': 'Ocean ',
        'context': 'Properties of  within the ocean component',
        'id': 'cmip6.ocean.',
        'sub-processes': ['ocean_',
                          'ocean_',
                          'ocean_'
                          ],
        'algorithms': ['ocean_',
                       'ocean_',
                       'ocean_'
                       ],
        }
    }
#
# Sub-processes
#

ocean_ = {
    'base': 'science.sub_process',
    'values': {
        'name': 'Ocean ',
        'context': 'Key properties of  in the ocean',
        'id': 'cmip6.ocean.',
        'details': ['ocean_',],
        }
    }

ocean_ = {
    'base': 'science.sub_process',
    'values': {
        'name': 'Ocean ',
        'context': 'Key properties of  in the ocean',
        'id': 'cmip6.ocean.',
        'details': ['ocean_',],
        }
    }

#
# Detailed processes properties
# NB: The difference between names and contexts is that the name is the name
# of the property detail, the context is the scientific context of the
# particular detail (which might just be a definition of the name).

ocean_ = {
    'base': 'science.detail',
    'values':
        {'context': 'Properties of  in ocean',
         'id': 'cmip6.ocean..details',
         'name': 'Properties of  in ocean',
         'select': 'scheme',
         'from_vocab': 'cmip6.ocean..types.%s' % version,
         'with_cardinality':'X.X',
         },
    'properties':
        [('ocean_','int','X.X',
          '<Decription>'),
         ]
    }

#
# CV for enumerated lists
#


ocean_ = {
    'name': 'Type of in ocean',
    'id': 'cmip6.ocean..types',
    'members': [
        ('', 'tbd'),
        ('', 'tbd'),
        ('', 'tbd'),
        ('', 'tbd'),
        ('', 'tbd'),
        ('Other', 'tbd'),
        ]
    }

ocean_ = {
    'name': 'Type of in ocean',
    'id': 'cmip6.ocean..types',
    'members': [
        ('', 'tbd'),
        ('', 'tbd'),
        ('', 'tbd'),
        ('', 'tbd'),
        ('', 'tbd'),
        ('Other', 'tbd'),
        ]
    }
