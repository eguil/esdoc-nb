ID = 'cmip6.seaice.si_properties'

TYPE = 'science.process'

CIM = '2.0'

CONTACT = ''

AUTHORS = ''

DATE = ''

VERSION = ''

# ====================================================================
# PROPERTIES
# ====================================================================
PROPERTIES = {

    # ----------------------------------------------------------------
    # MANIFEST
    # ----------------------------------------------------------------
    'short_name': 'Sea ice properties',
    'description': 'Collection of processes which control sea ice.',

    'details': ['base_properties',
                'thermodynamics_budget',
            ],

    'sub_process': ['thermodynamics',
                    'dynamics',
                    'radiative_processes',
                ],
    
    # ----------------------------------------------------------------
    # DETAILS
    # ----------------------------------------------------------------
    'base_properties': {
        'short_name': 'Sea Ice Properties',
        'description': 'Methods used to represent sea ice',
        'layering': (
            'ENUM:si_layering_types', '1.1',
        'new_ice_formation': (
            'str', '0.1',
            'Description of method used to form new ice'),
        'ice_lateral_melting': (
            'str', '0.1',
            'Method by whichg sea ice lateral melting occurs'),
        'ice_surface_sublimation': (
            'str', '0.1',
            'Method by which water sublimes on ice surface'),
        'water_ponds': (
            'str', '0.1',
            'Brief description of water ponding on ice'),
    },

    'thermodynamics_budget': {
        'short_name': 'Thermodynamics Budget',
        'description': 'Information required to close the thermodynamics budget',
    },
        
    # ----------------------------------------------------------------
    # SUB-PROCESSES
    # ----------------------------------------------------------------
    'thermodynamics': {
        'short_name': 'Sea Ice Thermodynamics',
        'description': 'Characteristics of sea ice thermodynamics processes',
        'details': {
            'thermo_processes': {
                'short_name':'Thermodynamic Process Details',
                'description': 'Information about basal heat flux and brine inclusions',
                'brine_inclusion_method': (
                    'ENUM:si_thermo_brine_types', '0.1',
                    'Brine Inclusion Methodology'),
                'basal_heat_flux': (
                    'str', '0.1',
                    'Method by which basal heat flux is handled'),
                'brine_inclusions': (
                    'str', '0.1',
                    'Method by which brine inclusions are handled'),
            },
            'snow_processes': {
                'short_name': 'Sea Ice Snow Processes',
                'description': 'Snow processes in sea ice thermodynamics',
                'types': (
                    'ENUM:si_snow_process_types', '1.N',
                    'Snow processes in sea ice thermodynamics'),
            },
            'vertical_heat_diffusion': {
                'short_name': 'Vertical Heat Diffusion',
                'description': 'Characteristics of vertical heat diffusion in sea ice.',
                'num_of_layers': (
                    'int', '1.1',
                    'Number of layers used for vertical heat diffusion'),
                'regular_grid': (
                    'bool', '0.1',
                    'If multiple layers, are they regularly distributed?'),
                'based_on_semtner': (
                    'bool','1.1',
                    'Is method based on semtner 1976?'),
            },
        },
    },

    'dynamics': {
        'short_name': 'Sea Ice Dynamics',
        'description': 'Characteristics of the Sea Ice Dynamics',
        'details': {
            'horizontal_advection': {
                'short_name': 'Horizontal Advection of Sea Ice',
                'description': 'Method of horizontal advection',
                'method': (
                    'ENUM:si_transport_methods', '0.1',
                    'Method of horizontal advection'),
            },
            'transport_in_thickness_space': {
                'short_name':'Sea Ice vertical transport',
                'description': 'Method of ice migration in thickness',
                'method': (
                    'ENUM:si_transport_methods', '0.1',
                    'Method of ice migration in thickness'),
            },
            'redistribution': {
                'short_name':'Sea Ice Redistribution',
                'description':'Additional processes which can redistribute sea ice.',
                'processes': (
                    'ENUM:si_redistribution_types', '0.N',
                    'Sea Ice Redistribution'),   
                'ice_strength_formulation': (
                    'str','0.1',
                    'Describe how ice-strength is formulated'),
            },
            'rheology': {
                'short_name': 'Sea Ice Rheology',
                'desription': 'Process by ',
                'method_of_ice deformation': (
                    'ENUM:si_rheology_types', '1.1',
                    'Sea Ice Rheology Type'),
            },
        },
    },
    
    'radiative_processes': {
        'short_name': 'Sea Ice Radiative Processes',
        'description': 'Collected properties of radiation in sea ice thermodynamics',
        'details': {
            'methods': {
                'short_name':'Sea Ice Radiative Process Details',
                'description':'Additional information about radiative processes in sea ice.',
                'surface_albedo': (
                    'str', '0.1',
                    'Method used to handle surface albedo'),
                'ice_radiation_transmission': (
                    'str', '0.1',
                    'Method by which solar radiation through ice is handled'),
            },
        },
    },

}

# ====================================================================
# ENUMERATIONS
# ====================================================================
ENUMERATIONS = {

    'si_thermo_brine_types': {
        'short_name': 'si thermo brine types',
        'description': 'Brine Inclusion Methodology',
        'members': [
            ('None', 'No brine inclusions included in sea ice thermodynamics'),
            ('Heat Reservoir', 'Brine inclusions treated as a heat reservoir'),
            ('Thermal Fixed Salinity', 'Thermal properties depend on S-T (with fixed salinity)'),
        ('Thermal Varying Salinity', 'Thermal properties depend on S-T (with varying salinity'),
        ]
    },

    'si_snow_process_types': { 
        'short_name': 'si snow process types',
        'description': 'Types of snow processes',
        'members': [
            ('single-layered heat diffusion', None),
            ('multi-layered heat diffusion', None),
            ('snow aging scheme', None),
            ('snow ice scheme', None),
        ]
    },

    'si transport_methods': {
        'short_name': 'si transport methods',
        'description': 'Transport Methods',
        'members': [
            ('Incremental Re-mapping','(including Semi-Lagrangian)'),
            ('Prather', None),
            ('Eulerian', None)
        ]
    },

    'si_redistribution_types': {
        'short_name': 'si redistribution types',
        'description':'Sea Ice Redistribution Types',
        'members': [
            ('Rafting', None),
            ('Ridging', None),
        ]
    },

    'si_rheology_types': {
        'short_name': 'si_rheology types',
        'description': 'Sea Ice rheology types',
        'members': [
            ('free-drift', None),
            ('Mohr-Coloumb', None),
            ('visco-plastic', None),
            ('elastic-visco-plastic','EVP'),
            ('granular', None),
            ('other', None)
        ]
    },
    
    'si_layering_types': {    
        'short_name': 'si layering types',
        'description': 'Sea Ice Layering Types',
        'members': [
            ('2-levels', 'Simulation uses two layers.'),
            ('Multi-level', 'Simulation uses more than two layers'),
            ('Ice-Types', 'Simulation does not use layers, but has multiple ice types per grid cell'),
        ]
    },
    
}

