ID = 'cmip6.ocean'

# CIM2 TYPE
TYPE = 'science.scientific_realm'

PROPERTIES = {
    # ----------------------------------------------------------------
    # PROPERTIES
    # ----------------------------------------------------------------
    'realm': 'Ocean',

    # ----------------------------------------------------------------
    # PROCESSES (in different files)
    # ----------------------------------------------------------------
    'processes': [
        'ocean_timestepping_framework',
        'ocean_advection',
        'ocean_lateral_physics',
        'ocean_vertical_physics',
        'ocean_uplow_boundaries',
        'ocean_boundary_forcing',
    ]
}

