ID = 'cmip6.atmosphere.cloud_scheme'

TYPE = 'science.process'

CIM = ''

CONTACT = ''

AUTHORS = ''

DATE = ''

VERSION = ''

# ====================================================================
# PROPERTIES
# ====================================================================
PROPERTIES = {

    # ----------------------------------------------------------------
    # MANIFEST
    # ----------------------------------------------------------------
    'short_name': 'Characteristics of the cloud scheme',
    'description': 'Characteristics of the cloud scheme',

    'details': ['separate_treatment',
                'cloud_overlap',
                'cloud_scheme_processes',
            ],
    
    'sub_process': ['sub_grid_scale_water_distribution',
                ],
    
    # ----------------------------------------------------------------
    # DETAILS
    # ----------------------------------------------------------------
    'separate_treatment': {
        'short_name': 'Cloud scheme separate treatment',
        'description': 'Different cloud schemes for the different types of clouds (convective, stratiform and boundary layer clouds).',
        'uses_separate_treatment': (
            'bool', '1.1',
            'Separate schemes for different cloud types'),
    },

    'cloud_overlap': {
        'short_name': 'Cloud overlap',
        'description': 'Method for taking into account overlapping of cloud layers.',
        'method': (
            'ENUM:cloud_scheme_cloud_overlap_method', '1.1',
             'Cloud scheme cloud overlap method'),
    }

    'cloud_scheme_processes': {
        'short_name': 'Cloud scheme processes',
        'description': 'Cloud processes included in the cloud scheme.',
        'processes_attributes': (
            'ENUM:cloud_scheme_processes_attributes', '1.N', 
            'Cloud scheme processes'),
    },
    
    # ----------------------------------------------------------------
    # SUB-PROCESSES
    # ----------------------------------------------------------------
    'sub_grid_scale_water_distribution': {
        'short_name': 'Sub-grid scale water distribution',
        'description': 'Sub-grid scale water distribution',
        'details': {
            'properties': {
                'short_name': 'Sub-grid scale water distribution',
                'description': 'Sub-grid scale water distribution',
                'type': (
                    'ENUM:cloud_scheme_sub_grid_scale_water_distribution_type', '1.1',
                    'Sub-grid scale water distribution type'),
                'function_name': (
                    'str', '1.1',
                    'Sub-grid scale water distribution function name'),
                'function_order': (
                    'int', '1.1',
                    'Sub-grid scale water distribution function type'),
                'convection_coupling': (
                    'ENUM:cloud_scheme_sub_grid_scale_water_distribution_convection', '1.N',
                    'Sub-grid scale water distribution coupling with convection'),
            },
        },
    },
  
}

# ====================================================================
# ENUMERATIONS
# ====================================================================
ENUMERATIONS = {

    'cloud_scheme_sub_grid_scale_water_distribution_type': {
    'short_name': 'Cloud scheme sub-grid scale water distribution type',
        'description': 'Approach used for cloud water content and fractional cloud cover',
        'members': [
            ('prognostic', None),
            ('diagnostic', None),
        ]
    },

    'cloud_scheme_sub_grid_scale_water_distribution_convection': {    
        'short_name': 'Cloud scheme sub-grid scale water distribution convection',
        'description': 'Type(s) of convection that the formation of clouds is coupled with',
        'members': [
            ('coupled with deep', None),
            ('coupled with shallow', None),
            ('not coupled with convection', None),
        ]
    },

    'cloud_scheme_cloud_overlap_method': {
        'short_name': 'Cloud scheme cloud overlap method',
        'description': 'Cloud scheme cloud overlap method',
        'members': [
            ('random', None),
            ('none', None),
            ('other', None),
        ]
    },

    'cloud_scheme_processes_attributes': {
        'short_name': 'Cloud scheme processes attributes',
        'description': 'Processes included in the cloud scheme.',
        'members': [
            ('entrainment', None),
            ('detrainment', None),
            ('bulk cloud', None),
            ('other', None),
        ]
    },

}
