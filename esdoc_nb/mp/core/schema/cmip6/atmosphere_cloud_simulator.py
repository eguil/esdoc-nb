ID = 'cmip6.atmosphere.cloud_simulator'

TYPE = 'science.process'

CIM = '2.0'

CONTACT = 'Charlotte Pascoe'

AUTHORS = ''

DATE = ''

VERSION = '0.9'

# ====================================================================
# PROPERTIES
# ====================================================================
PROPERTIES = {

    # ----------------------------------------------------------------
    # MANIFEST
    # ----------------------------------------------------------------
    'short_name': 'Atmosphere Cloud Simulator',
    'description': 'Characteristics of the cloud simulator',

    'details': ['isscp_attributes',
            ],

    'sub_process': ['cosp_attributes',
                    'inputs_radar',
                    'inputs_lidar',
                ],

    # ----------------------------------------------------------------
    # DETAILS
    # ----------------------------------------------------------------
    'isscp_attributes': {
        'short_name': 'Cloud simulation ISSCP attributes',
        'description': 'ISSCP Characteristics',
        'top_height': (
            'ENUM:cloud_simulator_isscp_top_height', '1.N',
            'Cloud simulator ISSCP top height'),
        'top_height_direction': (
            'ENUM:cloud_simulator_isscp_top_height_direction', '1.1',
            'Cloud simulator ISSCP top height direction'),
    },

    # ----------------------------------------------------------------
    # SUB-PROCESSES
    # ----------------------------------------------------------------
    'cosp_attributes': {
        'short_name': 'Cloud simulatot cosp attributes',
        'description': 'CFMIP Observational Simulator Package attributes',
        'details': {
            'properties': {
                'short_name': '',
                'description': '',
                'run_configuration': (
                    'ENUM:cloud_simulator_cosp_run_configuration', '1.1',
                    'Cloud simulator COSP run configuration'),
                'number_of_grid_points': (
                    'int', '1.1',
                    'Cloud simulator COSP number of grid points'),
                'number_of_columns': (
                    'int', '1.1',
                    'Cloud simulator COSP number of cloumns'),
                'number_of_levels': (
                    'int', '1.1',
                    'Cloud simulator COSP number of levels'),                
            },
        },
    },
    
    'inputs_radar': {
        'short_name': 'Cloud simulator input radar',
        'descrition': 'Characteristics of the cloud radar simulator',
        'details': {
            'properties': {
                'short_name': '',
                'description': '',
                'radar_frequency': (
                    'real', '1.1',
                    'Cloud simulator radar frequency'),
                'radar_type': (
                    'ENUM:cloud_simulator_inputs_radar_type', '1.1',
                    'Cloud simulator radar type'),
                'gas_absorption': (
                    'bool', '1.1',
                    'Cloud simulator radar uses gas absorption'),
                'effective_radius': (
                    'bool', '1.1',
                    'Cloud simulator radar uses effective radius'),
            },
        },
    },
    
    'inputs_lidar': {
        'short_name': 'Cloud simulator inputs lidar',
        'description': 'Characteristics of the cloud lidar simulator',
        'details': {
            'properties': {
                'short_name', '',
                'description': '',
            'ice_type': (
                'ENUM:cloud_simulator_inputs_lidar_ice_type', '1.1',
                'Cloud simulator lidar ice type'),
            'lidar_overlap': (
                'ENUM:cloud_simulator_inputs_lidar_overlap', '1.N',
                'Cloud simulator lidar overlap'),
            },
        },
    },
    
}

# ====================================================================
# ENUMERATIONS
# ====================================================================
ENUMERATIONS = {

    'cloud_simulator_isscp_top_height': {
        'short_name': 'Cloud simulator isscp top height',
        'description': 'Cloud top height management',
        'members': [
            ('no adjustment', None),
            ('IR brightness', None),
            ('visible optical depth', None),
        ]
    }

    'cloud_simulator_isscp_top_height_direction': {
        'short_name': 'Cloud simulator isscp top height direction',
        'description': 'Direction for finding the radiance determined cloud-top pressure. Atmosphere pressure level with interpolated temperature equal to the radiance determined cloud-top pressure.',
        'members': [
            ('lowest altitude level', None),
            ('highest altitude level', None),
        ]
    },

    'cloud_simulator_cosp_run_configuration': {
        'short_name': 'Cloud simulator cosp run configuration',
        'description': 'Method used to run the CFMIP Observational Simulator Package',
        'members': [
            ('Inline', None),
            ('Offline', None),
            ('None', None),
        ]
    },

    'cloud_simulator_inputs_radar_type': {
        'short_name': 'Cloud simulator inputs radar type',
        'description': 'Type of radar',
        'members': [
            ('surface', None),
            ('space borne', None),
        ]
    },

    'cloud_simulator_inputs_lidar_ice_type': {
        'short_name': 'Cloud simulator inputs lidar ice type',
        'description': 'Ice particle shape in lidar calculations',
        'members': [
            ('ice spheres', None),
            ('ice non-spherical', None),
        ]
    },

    'cloud_simulator_inputs_lidar_overlap': {
        'short_name': 'Cloud simulator inputs lidar overlap',
        'description': 'lidar overlap type',
        'members': [
            ('max', None),
            ('random', None),
            ('other', None),
        ]
    },

}
