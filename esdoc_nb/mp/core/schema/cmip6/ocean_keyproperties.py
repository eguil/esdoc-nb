__author__ = 'eguil, davidhassell'
version = '0.0.1'

ID = 'cmip6.ocean.key_properties'

## NOTE: This "ID" is surely debatable. I would like to see something
##       in the file which identifies the contents in the absence of
##       its position in a directory hierarchy. It will be easy to
##       validate this ID when the file *is* in a directory hierarchy.

PROPERTIES = {
    'type': 'science.key_properties',

    ## NOTE: The 'type' key is only time in this file when a CIM2 type
    ##       is mentioned ('science.key_properties' in this case). I
    ##       think we need to mention it somewhere - what if,
    ##       sometime, we wanted to have sub-processes in a different
    ##       file? We'd be glad of the 'type' key ...
    
    # ----------------------------------------------------------------
    # OVERVIEW
    # ----------------------------------------------------------------

    ## NOTE: The overview gives a quick look summary of the full
    ##       contents.
    
    'short_name': 'Ocean key properties',
    'description': 'Overview of key properties of ocean component',

    ## NOTE: Just trying out 'short_name' <=> 'name' and 
    ##       'description' <=> 'context'
    
    'grid'                         : ['grid'], # can be linked_to
    'resolution'                   : ['resolution'],
    'discretisation'               : ['discretisatsion'],
    'extra_conservation_properties': ['conservation_properties'],
    'tuning_applied'               : [],

    'details': ['basic_approximations',
                'prognostic_variables',
                'seawater_properties',
                'bathymetry',
                'nonoceanic_waters'],

    # ----------------------------------------------------------------
    # GRID, RESOLUTION and DISCRETISATION
    # ----------------------------------------------------------------

    'grid': {
        'short_name': 'NAME',
        'description': 'DESCRIPTION',
        'name': 'Name',
        'components': ['vertical_grid',
                       'horizontal_grid'],
    },

    ## NOTE: The type of the 'grid' does not need to be spelled out
    ##       because it is known from the ontology:
    ##       science.key_properties has a 'grid' property whose value
    ##       has to be a science.grid.
    
    'resolution': {
        'short_name': 'NAME',
        'description': 'DESCRIPTION',
        'thickness_of_surface_level': (
            'float', '0.1', 'Thickness in metres of surface ocean level (e.g. 0.1)'),
    },

    'discretisation': {
        'short_name': 'Type of discretisation scheme in ocean',
        'description': 'Type of discretisation scheme in ocean',
        'components': ['vertical_discretisation',
                       'horizontal_discretisation'],
    },

    'vertical_grid': {
        {'description': 'Properties of vertical coordinate in ocean',
         'short_name': 'Properties of vertical coordinate in ocean',
         'number_of_levels', ('int', '1.1', 'N v levs'),
    },

    'horizontal_grid': {
        {'description': 'Properties of H coordinate in ocean',
         'short_name': 'Properties of H coordinate in ocean',
         'type': ('ENUM:horiz_grid_type', '1,1'),
         'number_of_', ('int', '1.1', 'N v levs'),
         'number_of_xy_gridpoints': (
             'int', '0.1',
             'Total number of horizontal points on computational grid'),
    },

    'vertical_discretisation': {
        {'description': 'Properties of vertical coordinate in ocean',
         'short_name': 'Properties of vertical coordinate in ocean',
         'type', ('ENUM:vertical_coord_types', '1.1'),
         'partial_steps': (
             'bool', '1.1',
             'Using partial steps with Z or Z* vertical coordinate in ocean ?'),
    },

    ## NOTE: The 'type' property takes its value from an enum and
    ##       doesn't need a description (3rd element of tuple) because
    ##       the description is in the enum found in the ENUMS
    ##       dictionary below
        
    'horizontal_discretisation': {
        'short_name': 'Type of horizontal discretisation scheme in ocean',
        'description': 'Type of horizontal discretisation scheme in ocean',
        'scheme': ('ENUM:horiz_scheme_types', '1.1'),
        'ocean_pole_singularity_treatment': (
             'str', '1.1',
            'Describe how the North Pole singularity is treated (filter, pole rotation/displacement, artificial island, ...)'),
    },

    ## NOTE: The type of the 'horizontal_discretisation' does not need
    ##       to be spelled out because it is known from the ontology:
    ##       science.discretisation has a 'components' property whose
    ##       values have to be  science.discretisation_component
    
    # ----------------------------------------------------------------
    # CONSERVATION
    # ----------------------------------------------------------------    
    'conservation_properties': {
        'short_name': 'Ocean conservation properties',
        'description': 'Properties conserved in the ocean component',
        'conservation_scheme': ('ENUM:conservation_props_types', '1.N'),
        'ocean_conservation_method': (
            'char', '1.1', 'Describe how conservation properties are ensured in ocean'),
    },

    # ----------------------------------------------------------------
    # TUNING
    # ----------------------------------------------------------------    

    # ----------------------------------------------------------------
    # DETAILS
    # ----------------------------------------------------------------    
        
    'basic_approximations': {
        'description': 'List of basic approximations in the ocean component',
        'short_name': 'List of basic approximations in the ocean component',
        'basic_approximation_types': ('ENUM:basic_approx_types', '1.N'),
    },

    'nonoceanic_waters': {
        'short_name': 'Treatment of non-oceanic waters (isolated seas and river mouth)',
        'description': 'Describe if/how isolated seas and river mouth mixing or other specific treatment is performed',
        'ocean_nonoceanic_waters': (
            'str', '0.1',
            'Describe if/how isolated seas and river mouth mixing or other specific treatment is performed'),
    },
       
    'ocean_bathymetry': {
        'description':'Properties of bathymetry in ocean',
        'short_name': 'Properties of bathymetry in ocean', 
        'bathymetry_reference_dates': ('ENUM:ocean_bathymetry_ref_dates', '1.1'),
        'ocean_bathymetry_type': (
            'bool', '1.1',
            'Is the bathymetry fixed in time in the ocean ?'),
        'ocean_smoothing': (
            'str', '1.1',
            'Describe any smoothing or hand editing of bathymetry in ocean'),
        'ocean_bathymetry_source': (
            'str', '1.1',
            'Describe source of bathymetry in ocean'),
    },

    'prognostic_variables': {
        'description': 'List of prognostic variables in the ocean component',
        'short_name': 'List of prognostic variables in the ocean component',
        'prognostic_variables': ('ENUM:prognostic_vars_types', '1.N'),
    },

    'seawater_properties': {
       'description': 'Physical properties of seawater in ocean',
        'short_name': 'Properties of seawater in ocean', 
        'seawater_eos_type': ('ENUM:seawater_eos_types', '1.1'),
        'ocean_freezing_point': (
            'str', '1.1',
            'Describe freezing point in ocean (fixed or varying)'),
        'ocean_specific_heat': (
            'str', '1.1',
            'Describe specific heat in ocean (fixed or varying)'),x
    },
}
       

# ====================================================================
# ENUMERATED LISTS
# ====================================================================

ENUMS = {
    'ocean_basic_approx_types': {
        'name': 'Types of basic approximation in ocean',
        'members': [
            ('Primitive equations','tbd'),
            ('Non-hydrostatic', 'tbd'),
            ('Boussinesq', 'tbd'),
            ('Other', 'tbd')
        ],
    },
    
    
    'prognostic_vars_types': {
        'name': 'Types of basic approximation in ocean',
        'members': [
            ('Potential temperature','tbd'),
            ('Conservative temperature','tbd'),
            ('Salinity','tbd'),
            ('U-velocity','tbd'),
            ('V-velocity','tbd'),
            ('W-velocity','tbd'),
            ('SSH','Sea Surface Height'),
            ('Other', 'Other prognostic variables')
        ],
    },
    
    'seawater_eos_types': {
        'name': 'Types of seawater Equation of State in ocean',
        'members': [
            ('Linear','tbd'),
            ('Mc Dougall et al.', 'tbd'),
            ('Jackett et al. 2006', 'tbd'),
            ('TEOS 2010', 'tbd'),
            ('Other', 'tbd')
        ],
    },
    
    'bathymetry_ref_dates': {
        'name': 'List of reference dates for bathymetry in ocean',
        'members': [
            ('Present day','tbd'),
            ('21000 years BP', 'tbd'),
            ('6000 years BP', 'tbd'),
            ('LGM', 'Last Glacial Maximum'),
            ('Pliocene', 'tbd'),
            ('Other', 'tbd')
        ],
    },

    'horiz_scheme_types': {
        'name': 'Types of horizonal schemes in ocean',
        'members': [
            ('Finite difference / Arakawa B-grid','tbd'),
            ('Finite difference / Arakawa C-grid','tbd'),
            ('Finite difference / Arakawa E-grid','tbd'),
            ('Finite volumes', 'tbd'),
            ('Finite elements', 'tbd'),
            ('Other', 'tbd')
        ],
    },
    
    'vertical_coord_types': {
        'name': 'Types of vertical coordinates in ocean',
        'members': [
            ('Z-coordinate','tbd'),
            ('Z*-coordinate', 'tbd'),
            ('S-coordinate', 'tbd'),
            ('Isopycnic - sigma 0','Density referenced to the surface'),
            ('Isopycnic - sigma 2','Density referenced to 2000 m'),
            ('Isopycnic - sigma 4','Density referenced to 4000 m'),
            ('Isopycnic - other','Other density-based coordinate'),
            ('Hybrid / Z+S', 'tbd'),
            ('Hybrid / Z+isopycnic', 'tbd'),
            ('Hybrid / ALE', 'tbd'),
            ('Hybrid / other', 'tbd'),
            ('Pressure referenced (P)', 'tbd'),
            ('P*','tbd'),
            ('Z**', 'tbd'),
            ('Other', 'tbd')
        ],
    },

    'conservation_props_types': {
        'name': 'Types of conservation properties in ocean',
        'members': [
            ('Energy','tbd'),
            ('Enstrophy','tbd'),
            ('Salt', 'tbd'),
            ('Volume of ocean', 'tbd'),
            ('Other', 'tbd')
        ]
    },
}
