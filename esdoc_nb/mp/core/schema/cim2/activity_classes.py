
def activity():
    """ Base class for activities. """
    return {
        'type': 'class',
        'base': None,
        'is_abstract': True,
        'properties': [
            ('name', 'str', '1.1',
                'Short name or abbreviation.'),
            ('long_name', 'str', '0.1',
                'Longer version of activity name'),
            ('canonical_name', 'str', '0.1',
                'Community defined identifier or name'),
            ('keywords', 'str', '0.N',
                'User defined keywords'),
            ('rationale', 'shared.cimtext', '0.1',
                'Explanation of why this activity was carried out and/or what it was intended to achieve.'),
            ('description', 'shared.cimtext', '0.1',
                'Description of what is to be done (or was done).'),
            ('responsible_parties', 'shared.responsibility', '0.N',
                'People or organisations responsible for activity'),
            ('references', 'shared.reference', '0.N',
                'Relevant documentation'),
            ('meta', 'shared.meta','1.1',
                'Metadata describing how this document was created'),
            ('duration', 'shared_time.time_period', '0.1',
                'Time the activity was (or will be) active'),
            ],
        }


def parent_simulation():
    """ Defines the relationship between a simulation and its parent"""
    return {
        'type': 'class',
        'base': None,
        'is_abstract' : False,
        'properties':[
            ('parent','linked_to(data.simulation)','1.1','The parent simulation of this child.'),
            ('branch_time_in_parent','shared_time.date_time','0.1',
                'The time in parent simulation calendar at which this simulation was branched'),
            ('branch_time_in_child','shared_time.date_time','0.1',
                'The time at which the present simulation started in the child calendar'),
        ]
    }


def conformance():
    """A specific conformance. Describes how a particular numerical requirement has been implemented.
    Will normally be linked from an ensemble descriptor. """
    return {
        'type': 'class',
        'base': 'activity.activity',
        'is_abstract': False,
        'properties': [
            ('target_requirement', 'linked_to(designing.numerical_requirement)', '1.1',
                'URI of the target numerical requirement'),
        ],
        'constraints':[
            ('rationale', 'hidden'),
            ('duration', 'hidden'),
            ('canonical_name', 'hidden'),
            ('keywords', 'hidden'),
            ('description', 'cardinality', '1.1')
        ]
    }


def ensemble():
    """Generic ensemble definition.
    Holds the definition of how the various ensemble members have been configured.
    If ensemble axes are not present, then this is either a single member ensemble,
    or part of an uber ensemble."""
    return {
        'type': 'class',
        'base': 'activity.activity',
        'is_abstract': False,
        'properties': [
            ('supported', 'linked_to(designing.numerical_experiment)', '1.N',
                'Experiments with which the ensemble is associated (may differ from constituent simulations)'),
            ('part_of', 'activity.uber_ensemble', '0.N',
                'Link to one or more over-arching ensembles that might includes this one'),
            ('common_conformances', 'linked_to(activity.conformance)', '0.N',
                'Conformance documents for requirements common across ensemble.'),
            ('has_ensemble_axes', 'activity.ensemble_axis', '0.N',
                "Set of axes for the ensemble"),
            ('members', 'activity.ensemble_member', '1.N',
                "The set of ensemble members"),
            ('documentation', 'linked_to(shared.online_resource)', '0.N',
                "Links to web-pages and other ensemble specific documentation (including workflow descriptions)")
            ],
        'constraints':[
            ('rationale','hidden'),
            ('canonical_name', 'hidden'),
            ('keywords', 'hidden'),
            ('duration','hidden')
        ]
    }


def uber_ensemble():
    """An ensemble made up of other ensembles. Often used where parts of an ensemble were run by
    different institutes. Could also be used when a new experiment is designed which can use
    ensemble members from previous experiments and/or projects.
    """
    return {
        'type': 'class',
        'base': 'activity.ensemble',
        'is_abstract': False,
        'properties': [
            ('child_ensembles','linked_to(activity.ensemble)','1.N',
                'Ensemble which are aggregated into this one'),
        ],
        'constraints': [
            ('has_ensemble_axes','cardinality','1.N'),
            ('common_conformances','hidden'),
            ('members','hidden'),
        ]
    }


def ensemble_axis():
    """Defines the meaning of r/i/p indices in an ensemble """
    return {
        'type': 'class',
        'base': None,
        'is_abstract': False,
        'pstr': ('%s', ('axis', )),
        'properties': [
            ('short_identifier','str','1.1',
                'e.g. "r" or "i" or "p" to conform with simulation ensemble variant identifiers'),
            ('target_requirement','linked_to(designing.numerical_requirement)','1.1',
                'URI of the target numerical requirement'),
            ('extra_detail','shared.cimtext','1.1',
                'Any extra detail required to describe how this ensemble axis was delivered'),
            ('member', 'activity.axis_member', '1.N', 'Individual member descriptions along axis'),
            ],
        }

def ensemble_member():
    """ An ensemble may be a complicated interplay of axes, for example, r/i/p, not all of which
    are populated, so we need a list of the actual simulations and how they map onto the ensemble
    axes.
    """
    return {
        'type': 'class',
        'base': None,
        'is_abstract': False,
        'properties': [
            ('simulation', 'linked_to(data.simulation)', '1.1',
                'Actual simulation description for an ensemble member.'),
            ('variant_id','str','1.1','A string which concatenates axis member short identiers (e.g r1i1p1f1) '),
            ('ran_on', 'linked_to(platform.machine)', '0.1',
                'The machine on which the simulation was run'),
            ('had_performance', 'linked_to(platform.performance)', '0.1',
                'Performance of the simulation'),
            ('errata', 'linked_to(shared.online_resource)', '0.1',
                'Link to errata associated with this simulation')
            ]
        }


def axis_member():
    """ Description of a given ensemble member. It will normally be related to a specific
    ensemble requirement. Note that start dates can be extracted directly from the simulations
    and do not need to be recorded with an axis member description. """
    return {
        'type': 'class',
        'base': None,
        'is_abstract': False,
        'properties': [
            ('index', 'int', '1.1', 'The ensemble member index'),
            ('description', 'str', '1.1', 'Description of the member (or name of parameter varied)'),
            ('value', 'float', '0.1', 'If parameter varied, value thereof for this member'),
            ('extra_detail','shared.cimtext','0.1',
                'If necessary: further information about ensemble member conformance.')
            ],
    }




