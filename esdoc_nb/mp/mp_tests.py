import copy
import unittest
from datetime import datetime

from core.lib.factory import cimFactory, convert_from_pyesdoc, make_cim_instance, makeQuickText
from core.lib.meta import get_from_modules, checkmetamodel, PropertyType
from core.lib.uml import isCIMinstance
from core.lib.uml import umlProperty, umlClass, umlEnum

from esdoc_nb.mp.core.lib.definitions import class_packages, enum_packages, classmap
# vocab imports follow
from esdoc_nb.mp.core.schema import cmip6

__author__ = 'BNL28'


# FIXME: Need to introduce support for a constraint language
# Currently want support for
# cardinality and value constraints.


class TestFactory(unittest.TestCase):
    def testFactory(self):
        f = cimFactory()
        for c in ['Cimtext',]:
            assert c in f.classes, 'Class Failure %s not in %s' % (c, sorted(f.classes.keys()))
        for e in ['GridTypes',]:
            assert e in f.enums, 'Enum Failure %s not in %s' % (e, sorted(f.enums.keys()))

    def testDateTime(self):
        """ Something odd happens when we build a CIM DateTime"""
        x = make_cim_instance('DateTime')
        self.assertEqual(len(x.attributes), 2)

    def test_getbase(self):
        """ test finding base classes"""
        f = cimFactory()
        b = f.base('NumericalExperiment')
        self.assertEqual(b, ['Activity'])

    def NOtest_expandclasses(self):
        """ Test expanding classes and exploiting extension points"""
        #FIXME: Need to consider what we're doing here
        f = cimFactory()
        constructors = cmip6.get_constructors()
        f.add_package_by_functions('cmip6', constructors)
        self.assertIn('Cmip6Model', f.fullset)
        m = make_cim_instance('Model', f)
        self.assertEqual('Cmip6Model', m.cimType)


class TestMeta(unittest.TestCase):
    """ Establish basic meta structures"""

    def setUp(self):
        self.factory = cimFactory()

    def testDocumentList(self):
        """ Compare list of documents by inspection with document enumeration"""
        doc_list_by_inspection = []
        for k in self.factory.classes:
            if self.factory.isdoc(k):
                doc_list_by_inspection.append(k)
        doc_enum = self.factory.build('DocumentTypes')
        doc_list_by_definition = [k for k in doc_enum.members.keys()]
        # Now sort
        inspection = sorted(doc_list_by_inspection)
        defined = sorted(doc_list_by_definition)
        print inspection, defined
        self.assertEqual(inspection, defined,
                         'Document classes not consistent between inspection (%s) and definition (%s)'
                         % (defined, inspection))

    def testSetupMeta(self):
        author = make_cim_instance('Party')
        authorname = 'Bryan'
        author.name = authorname
        ensemble = self.factory.build('Ensemble', initialise_metadata=True, author=author)
        self.assertEqual(ensemble.meta.metadata_author.name, authorname)


class TestProperty(unittest.TestCase):

    def setUp(self):
        self.p = ('long_name', PropertyType('str'), '0.1', 'Additional alternative long name')
        self.q = ('name', PropertyType('str'), '1.1', 'Common name of machine')
        self.r = ('experiments', PropertyType('linked_to(activity.NumericalExperiment)'), '1.N',
                  'Experiments required/requested within the project')
        self.umlp = umlProperty(self.p)
        self.umlq = umlProperty(self.q)
        self.umlr = umlProperty(self.r)

    def testReadonlyProperty(self):
        """ Test cim UML properties are readonly in property instances"""
        with self.assertRaises(AttributeError):
            self.ump.name = 'fred'

    def testBasicAPI(self):
        """Basic API test"""
        apiout = (self.umlp.name, PropertyType(self.umlp.target), self.umlp.cardinality, self.umlp.doc)
        self.assertEqual(apiout, self.p)

    def testSingleValued(self):
        """ Check single valued does what we think it should"""
        self.assertTrue(self.umlp.single_valued)
        self.assertTrue(self.umlq.single_valued)
        self.assertFalse(self.umlr.single_valued)

    def testSetter(self):
        """Test we can assign values"""
        name = 'ARCHER'
        self.umlq.set(name)
        self.assertEqual(self.umlq.value, name)

    def testValidSetting(self):
        """ Ensure we can't set the wrong types to properties """
        self.assertRaises(ValueError, self.umlp.set, 1.0)

    def testMultiValueSetting(self):
        """ Test we can't set a multivalued property"""
        e = make_cim_instance('NumericalExperiment')
        self.assertRaises(ValueError, self.umlr.set, e)

    def testlinkage(self):
        """ Test the islink property """
        self.assertTrue(self.umlr.islink)
        self.assertFalse(self.umlp.islink)

    def testReferenceStereotype(self):
        """ i.e that we see an online resource for linked_to objects
        rather than the actual type"""
        # FIXME put a test here
        pass

    def testStrProperty(self):
        """Ensure that a proper string version is possible"""
        repr(self.umlp)


class TestMethods(unittest.TestCase):
    def testIsDocument(self):
        """Simple test: these should be documents"""
        f = cimFactory()
        x = ['Party','NumericalExperiment','Simulation']
        for c in x:
            self.assertTrue(f.isdoc(c),' Failed with %s' % c)

    def testIsNotDocument(self):
        """Simple test: these should not be documents"""
        x = ['DateTime', 'Cimtext']
        f = cimFactory()
        for c in x:
            r = f.isdoc(c)
            self.assertFalse(r, '[%s] should not be reported as a document' % c)

    def testIsCIMInstance(self):
        """ Make sure we can assign a subclass to a thing which expects as superclass"""
        x = make_cim_instance('Party')
        y = make_cim_instance('MinimalMeta')
        x.meta = y
        z = make_cim_instance('Meta')
        self.assertTrue(isCIMinstance(z, 'MinimalMeta'))
        x.meta = z

    def testInstanceID(self):
        """ Test expectation that things are cimClasses and cimEnums"""
        c = make_cim_instance('NumericalExperiment')
        assert isinstance(c, umlClass)
        e = make_cim_instance('TextCode')
        assert isinstance(e, umlEnum)


class TestMyClasses(unittest.TestCase):
    """ Build and test all classes can be instantiated """

    def __testmaker(self, t):
        """ Wrap around a make so we know what it is if it goes wrong """
        try:
            x = make_cim_instance(t)
        except:
            print 'Unable to make %s' % t
            x = make_cim_instance(t)
        for y in x.attributes:
            p = x.attributes[y]
            if p.target not in classmap:
                try:
                    z = make_cim_instance(p.target)
                except:
                    print y
                    print 'Failing with property %s in %s' % (p.target, t)
                    z = self.__testmaker(p.target)
                self.assertTrue(isCIMinstance(z, p.target), 'Failed with ' + p.target)
        return x

    def testAllClasses(self):
        """Test instantiating all known CIM classes"""
        factory = cimFactory()
        for k in factory.classes:
            checkmetamodel(factory.classes[k], factory.fullset)
            x = self.__testmaker(k)
            self.assertTrue(isCIMinstance(x, k), 'failed with %s' % k)

    def testCardinalityConstraint(self):
        """Test cardinality works"""
        p = make_cim_instance('Project')
        d = p.attributes['description']
        self.assertEqual('1.1', d.cardinality)

    def testHiddenConstraint(self):
        """ Test hiding a superclass property from a subclass """
        nr = make_cim_instance('NumericalRequirement')
        print nr.attributes
        self.assertNotIn('duration', nr.attributes)

    def testValidate(self):
        """ Make sure that thing are marked invalid until
        all attributes are present"""
        m= make_cim_instance('Project')
        m.name = 'test'
        meta = make_cim_instance('Meta')
        m.meta = meta
        self.assertTrue(not m.validate(True))
        with self.assertRaises(ValueError):
            m.validate()
        m.description = makeQuickText('abc')
        self.assertTrue(m.validate())

    def testDeepcopy(self):
        """ What does deepcopy do with these classes?"""
        m = make_cim_instance('Meta')
        p = make_cim_instance('Project')
        p.meta = m
        p.name = 'name1'
        x = copy.deepcopy(p)
        self.assertEqual(x, p)
        p.name = 'name2'
        self.assertNotEqual(x, p)

    def testAlternativesOfSubClasses(self):
        ''' Test that self type is not in alternatives of subclasses '''
        stc = make_cim_instance('TemporalConstraint')
        print 'Alternatives', stc.alternatives
        self.assertEqual('TemporalConstraint' not in stc.alternatives, True)
        self.assertEqual('NumericalRequirement' in stc.alternatives, True)

    def testAlternatives(self):
        ''' Make sure all alternative classes are subclasses of parent '''
        factory = cimFactory()
        for c in factory.classes:
            d = factory.classes[c]
            if 'alternatives' in d:
                for a in d['alternatives']:
                    aa = make_cim_instance(a)
                    dd = make_cim_instance(d['cimType'])
                    ok = isCIMinstance(aa, dd.cimType)
                    self.assertTrue(ok, 'Failing on %s %s' % (a, d['cimType']))

    def testMetaLastUpdated(self):
        m= make_cim_instance('Meta')
        m.metadata_last_updated = datetime.now().isoformat(' ')



class TestEnums(unittest.TestCase):

    def testAllEnums(self):
        ''' Test instantiating all known CIM enums '''
        f = cimFactory()
        for k in f.enums:
            try:
                make_cim_instance(k)
            except:
                print 'Failing to make %s' % k
                make_cim_instance(k)

    def testEnumSetting(self):
        f = cimFactory()
        tp = f.build('TimeUnits')
        self.assertRaises(ValueError, tp.set, 'bnl')
        tp.set('days')
        self.assertEqual(str(tp), 'days')

    def NOtest_cmip6_enums(self):
        f = cimFactory()
        f.add_extension_package('cmip6')
        self.assertIn('SiLayeringTypes', f.enums)

    def NOtest_cmip6_class(self):
        f = cimFactory()
        f.add_extension_package('cmip6')
        self.assertIn('SiSnowProcesses', f.classes)
        x = f.build('SiSnowProcesses')
        self.assertTrue(isCIMinstance(x,'ProcessDetail'))


class MiscTests(unittest.TestCase):

    def testStringFormat(self):
        """ Test simple pstr use case """
        tp = make_cim_instance('TimePeriod')
        u = make_cim_instance('TimeUnits')
        u.set('days')
        tp.length = 10
        tp.units = u
        self.assertEqual(str(tp)[0:7], '10 days')

    def testQuickText(self):
        m = make_cim_instance('Model')
        m.name = 'FastModel'
        m.description = makeQuickText('Dummy model')
        self.assertTrue(isCIMinstance(m.description, 'Cimtext'))


class SerialisationTests(unittest.TestCase):

    def setUp(self):
        """ Provide some re-usable instances for the tests"""
        f = cimFactory()
        m = make_cim_instance('MinimalMeta', f)
        a = make_cim_instance('Party', f)
        r = make_cim_instance('Responsibility', f)
        rc = make_cim_instance('RoleCode', f)
        rc.set('author')
        a.name = 'Author Test'
        a.meta = m
        r.party.append(a)
        r.role = rc
        self.author = r
        numexp = f.build('NumericalExperiment')
        numexp.name = 'An experiment'
        numexp.responsible_parties.append(self.author)
        self.numexp = numexp

    def NOtest_pyesdoc_single(self):
        """ Test that one attribute in an mp class instance can be converted to pyesdoc"""
        x = self.numexp
        y = x.pyesdoc
        self.assertEqual(y.name, x.name)

    def NOtest_twoway(self):
        """ Tests simple two way conversion for entire class instance"""
        x = self.numexp
        y = x.pyesdoc
        z = convert_from_pyesdoc(y)
        self.assertEqual(z, x)

    def NOtest_pyesdoc_all(self):
        """ Test all classes can be converted to pyesdoc"""
        factory = cimFactory()
        for k in factory.classes:
            x = factory.build(k)
            y = x.pyesdoc
            z = convert_from_pyesdoc(y)
            self.assertEqual(z,x,'Cannot two way convert class %s' % k)


class PackageTest(unittest.TestCase):
    """ Test that all properties exist in the correct package"""
    def test_packages(self):
        """ Check properties"""
        constructors, packages = get_from_modules(class_packages)
        enum_constructors, e_packages = get_from_modules(enum_packages)
        results = []
        for pkg in packages:
            # looping over all constructors in the package
            for k in packages[pkg]:
                constructor = constructors[k]
                # only need to loop over classes not enums
                if constructor['type'] == 'class':
                    if 'properties' in constructor:
                        for p in constructor['properties']:
                            target = p[1]
                            target_pkg, target_klass = target.package, target.camel
                            if target_pkg is None:
                                if target_klass in packages[pkg]:
                                    pass
                                elif pkg in e_packages and target_klass in e_packages[pkg]:
                                    pass
                                elif target_klass in classmap:
                                    pass
                                else:
                                    results.append('%s, %s, %s, %s' % (pkg, k, target_pkg, target_klass))
                            else:
                                if target_pkg in e_packages:
                                    if target_klass in e_packages[target_pkg]:
                                        pass
                                elif target_klass not in packages[target_pkg]:
                                    results.append ('%s, %s, %s, %s, %s, %s' % (pkg, k, p[0],
                                                target_pkg, target_klass, packages[target_pkg]))


if __name__ == "__main__":
    unittest.main()


