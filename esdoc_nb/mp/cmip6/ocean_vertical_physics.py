__author__ = 'eguil'
version = '0.0.1'

#
# CMIP6 ocean vertical physics CV
#
# Version history on git/bitbucket#
#
# Top level process
#
ocean_ = {
    'base': 'science.process',
    'values': {
        'name': 'Ocean vertical physics',
        'context': 'Properties of  vertical physics within the ocean component',
        'id': 'cmip6.ocean.vertical.physics',
        'sub-processes': ['ocean_vertphys_attributes',
                          'ocean_boundary_layer_mixing',
                          'ocean_interior_mixing'],
        }
    }
#
# Sub-processes
#

ocean_vertphys_attributes = {
    'base': 'science.sub_process',
    'values': {
        'name': 'Ocean vertical physics attributes',
        'context': 'General properties of vertical physics in the ocean',
        'id': 'cmip6.ocean.vertphys.attributes',
        'details': ['ocean_vertphys_props',],
        }
    }

ocean_boundary_layer_mixing = {
    'base': 'science.sub_process',
    'values': {
        'name': 'Ocean boundary layer mixing',
        'context': 'Key properties of boundary layer mixing in the ocean (aka mixed layer)',
        'id': 'cmip6.ocean.boundary.layer.mixing',
        'details': ['ocean_bndlayer_mixing_tracers','ocean_bndlayer_mixing_momentum'],
        }
    }

#
# Detailed processes properties
# NB: The difference between names and contexts is that the name is the name
# of the property detail, the context is the scientific context of the
# particular detail (which might just be a definition of the name).

ocean_vertphys_props = {
    'base': 'science.process_detail',
    'values':
        {'context': 'Properties of vertical physics in ocean',
         'id': 'cmip6.ocean.vertphys.props.details',
         'name': 'Properties of vertical physics in ocean',
         'select': 'scheme',
         'from_vocab': 'cmip6.ocean.vertphys.convection.types.%s' % version,
         'with_cardinality':'1.1',
         },
    'properties':
        [('ocean_tide_induced_mixing','char','1.1',
          'Describe how tide induced mixing is modelled (barotropic, baroclinic, none)'),
         ('ocean_langmuir_cells_mixing','bool','1.1',
          'Is there Langmuir cells mixing in upper ocean ?'),
         ]
    }

ocean_bndlayer_mixing_tracers = {
    'base': 'science.process_detail',
    'values':
        {'context': 'Properties of boundary layer mixing on tracers in ocean',
         'id': 'cmip6.ocean.bndlayer.mixing.tracers.details',
         'name': 'Properties of boundary layer mixing on tracers in ocean',
         'select': 'scheme',
         'from_vocab': 'cmip6.ocean.bndayer.mixing.types.%s' % version,
         'with_cardinality':'1.1',
         },
    'properties':
        [('ocean_OML_tracers_turbulent_closure_order','float','0.1',
          'If turbulent BL mixing of tracers, specific order of closure (0, 1, 2.5, 3)'),
         ('ocean_OML_tracers_constant','int','0.1',
          'If constant BL mixing of tracers, specific coefficient (m2/s)'),
         ('ocean_OML_tracers_background','char','1.1',
          'Background BL mixing of tracers coefficient, (schema and value in m2/s - may by none)'),
         ]
    }

ocean_bndlayer_mixing_momentum = {
    'base': 'science.process_detail',
    'values':
        {'context': 'Properties of boundary layer mixing on momentum in ocean',
         'id': 'cmip6.ocean.bndlayer.mixing.momentum.details',
         'name': 'Properties of boundary layer mixing on momentum in ocean',
         'select': 'scheme',
         'from_vocab': 'cmip6.ocean.bndayer.mixing.types.%s' % version,
         'with_cardinality':'1.1',
         },
    'properties':
        [('ocean_OML_momentum_turbulent_closure_order','float','0.1',
          'If turbulent BL mixing of momentum, specific order of closure (0, 1, 2.5, 3)'),
         ('ocean_OML_momentum_constant','int','0.1',
          'If constant BL mixing of momentum, specific coefficient (m2/s)'),
         ('ocean_OML_momentum_background','char','1.1',
          'Background BL mixing of momentum coefficient, (schema and value in m2/s - may by none)'),
         ]
    }

#
# CV for enumerated lists
#


ocean_bndayer_mixing_types = {
    'name': 'Types of boundary layer mixing in ocean',
    'id': 'cmip6.ocean.bndayer.mixing.types',
    'members': [
        ('Constant value', 'tbd'),
        ('Turbulent closure - TKE', 'tbd')
        ('Turbulent closure - KPP', 'tbd')
        ('Turbulent closure - Mellor-Yamada', 'tbd')
        ('Turbulent closure - Bulk Mixed Layer', 'tbd')
        ('Richardson number dependent - PP', 'tbd'),
        ('Richardson number dependent - KT', 'tbd'),
        ('Imbeded as isopycnic vertical coordinate', 'tbd')
        ('Other', 'tbd')
        ]
    }
